/** WordCloud.h
 * ===========================================================
 * Name: CS220, Spring 2019
 *
 * Purpose:  Produces a word cloud given a word list and thier
 * 			 frequency.
 * ===========================================================*/

#ifndef WORDCLOUD_H
#define WORDCLOUD_H

#include "CountedMap.h"

int buildWordCloud(int *argWordCounts, STRING*argWords, STRING argFileName, int argNumWords);


#endif // !WORDCLOUD_H

