//
// Created by C21David.Thacker on 9/19/2018.
//

#include "lab14functs.h"

int main(){
    int attempts[MAXPLAYERS];
    int yards[MAXPLAYERS];
    int touchdowns[MAXPLAYERS];
    int numEntries = readData(attempts,yards,touchdowns);
    printf("%d players data read.\n", numEntries);
    printf("The player ID %d is %s and he scored %d touchdowns in 2017.\n", 0, getPlayerName(0), touchdowns[0]);
    printf("The player ID %d is %s and he rushed for %d yards in 2017.\n", 122, getPlayerName(122), yards[122]);

    int maxYardsID = findMaxIndex(yards, numEntries);
    printf("The player with the most yards in 2017 is %s and he had %d.\n", getPlayerName(maxYardsID), yards[maxYardsID]);

    int maxTDSID = findMaxIndex(touchdowns, numEntries);
    printf("The player with the most TDs in 2017 is %s and he had %d.\n", getPlayerName(maxTDSID), touchdowns[maxTDSID]);

    int thresholdTDS = 10;
    int thresholdYards = 1000;
    int thresholdAttempts = 100;
    int numPlayersOverThresholdTDS = numPlayerOverX(touchdowns, numEntries, thresholdTDS);
    int numPlayersOverThresholdYards = numPlayerOverX(yards, numEntries, thresholdYards);
    int numPlayersOverThresholdAttempts = numPlayerOverX(attempts, numEntries, thresholdAttempts);
    printf("The number of players with more than %d TDs in 2017 is %d.\n", thresholdTDS, numPlayersOverThresholdTDS);
    printf("The number of players with more than %d yards in 2017 is %d.\n", thresholdYards, numPlayersOverThresholdYards);
    printf("The number of players with more than %d attempts in 2017 is %d.\n", thresholdAttempts, numPlayersOverThresholdAttempts);

    int thresholdYardsCarries = 100;
    int yardsPerCarryID = getMaxYardsPerCarryIndex(yards, attempts, numEntries, thresholdYardsCarries);
    printf("The player with the most yards per carry (>%d attempts) is %s and he had %.1lf.\n", thresholdYardsCarries,getPlayerName(yardsPerCarryID),getMaxYardsPerCarry(yards, attempts, yardsPerCarryID));
    thresholdYardsCarries = 50;
    yardsPerCarryID = getMaxYardsPerCarryIndex(yards, attempts, numEntries, thresholdYardsCarries);
    printf("The player with the most yards per carry (>%d attempts) is %s and he had %.1lf.\n", thresholdYardsCarries,getPlayerName(yardsPerCarryID),getMaxYardsPerCarry(yards, attempts, yardsPerCarryID));
    thresholdYardsCarries = 10;
    yardsPerCarryID = getMaxYardsPerCarryIndex(yards, attempts, numEntries, thresholdYardsCarries);
    printf("The player with the most yards per carry (>%d attempts) is %s and he had %.1lf.\n", thresholdYardsCarries,getPlayerName(yardsPerCarryID),getMaxYardsPerCarry(yards, attempts, yardsPerCarryID));
}