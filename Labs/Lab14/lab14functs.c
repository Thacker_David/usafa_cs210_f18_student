//
// Created by C21David.Thacker on 9/19/2018.
//

int findMaxIndex(int array[],int numEntries){
    int bestPlayerID = 0;
    int maxScore = 0;
    for(int i = 0; i < numEntries; i++){
        if(array[i] >= maxScore){
            maxScore = array[i];
            bestPlayerID = i;
        }
    }
    return bestPlayerID;
}

int numPlayerOverX(int array[], int numEntries, int threshold){
    int numPlayersOver = 0;
    for(int i = 0; i < numEntries; i++){
        if(array[i] >= threshold){
            numPlayersOver++;
        }
    }
    return numPlayersOver;
}

int getMaxYardsPerCarryIndex(int array[], int array2[], int numEntires, int threshold){
    int maxYardsPerCarryID = 0;
    double maxYardsPerCarry = 0;
    for(int i = 0; i < numEntires; i++){
        if(array2[i] >= threshold){
            if((((double)array[i])/array2[i]) >= maxYardsPerCarry){
                maxYardsPerCarry = ((double) array[i]/array2[i]);
                maxYardsPerCarryID = i;
            }
        }
    }
    return maxYardsPerCarryID;
}

double getMaxYardsPerCarry(int array[], int array2[], int iD){
    return (double) array[iD] / array2[iD];
}

