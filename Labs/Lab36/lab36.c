/** lab36.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Documentation: YOUR DOC STATEMENT
* ===========================================================  */

#include "lab36.h"

int main() {
    // Exercise 1
    printf("\nExercise 1: sum_squares()\n\n");
    
    
    
    // Exercise 2
    printf("\nExercise 2: is_even()\n\n");
    
    
    
    // Exercise 3
    printf("\nExercise 3: power_two()\n\n");
    
    
    
    // Exercise 4
    printf("\nExercise 4: reverse_bytes()\n\n");
    
    
    
    // Exercise 5
    printf("\nExercise 5: is_palindrome()\n\n");

    return 0;
}

int sum_squares(int N){
    if(N == 1){
        return 1;
    } else{
        return N*N + sum_squares(N-1);
    }
}

int is_even(int x){
    if(x&1){
        return 0;
    }
    else{
        return 1;
    }
}

int reverse_bytes(unsigned int val){
    int swapped = 0;
    swapped = ((val >> 24) & 0xff) | // 3 to 0
            ((val << 8) & 0xff0000) | // 1 to 2
            ((val >> 8) & 0xff00) | // 2 to 1
            ((val << 24) & 0xff000000); // 0 to 3
    return swapped;
}

int power_two(int N){
    return 2 << (N-1);
}

int is_palindrome(char message[], int len){
    if(len < 1){
        return 1;
    }
    else if(message[0] != message[len-1]){
        return 0;
    }
    else{
        return is_palindrome(message+1, len-2);
    }
}