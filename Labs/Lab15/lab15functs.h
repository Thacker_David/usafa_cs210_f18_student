//
// Created by C21David.Thacker on 9/19/2018.
//

#ifndef MYEXE_LAB15FUNCTS_H
#define MYEXE_LAB15FUNCTS_H
#include <stdio.h>
#include "lab15fbFuncts.h"
int findMaxIndex(int array[][DATACOLS], int numEntries, int colToSearch);
int numPlayerOverX(int array[][DATACOLS], int numEntries, int threshold,int colToSearch);
int getMaxYardsPerCarryIndex(int array[][DATACOLS], int numEntires, int threshold);
double getMaxYardsPerCarry(int array[][DATACOLS], int iD);
#endif //MYEXE_LAB15FUNCTS_H
