//
// Created by C21David.Thacker on 9/19/2018.
//

#ifndef MYEXE_LAB14FUNCTS_H
#define MYEXE_LAB14FUNCTS_H
#include <stdio.h>
#include "lab14fbFuncts.h"
int findMaxIndex(int array[], int numEntries);
int numPlayerOverX(int array[], int numEntries, int threshold);
int getMaxYardsPerCarryIndex(int array[], int array2[], int numEntires, int threshold);
double getMaxYardsPerCarry(int array[], int array2[], int iD);
#endif //MYEXE_LAB14FUNCTS_H
