//
// Created by C21David.Thacker on 9/19/2018.
//

#include "lab15functs.h"

int main(){
    int playerStats[MAXPLAYERS][DATACOLS];
    int numEntries = readData(playerStats);
    printf("%d players data read.\n", numEntries);
    printf("The player ID %d is %s and he scored %d touchdowns in 2017.\n", 0, getPlayerName(0), playerStats[0][2]);
    printf("The player id %d is %s and he rushed for %d yards in 2017.\n", 122, getPlayerName(122), playerStats[122][1]);

    int maxYardsID = findMaxIndex(playerStats, numEntries, 1);
    printf("The player with the most yards in 2017 is %s and he had %d.\n", getPlayerName(maxYardsID), playerStats[maxYardsID][1]);

    int maxTDSID = findMaxIndex(playerStats, numEntries, 2);
    printf("The player with the most TDs in 2017 is %s and he had %d.\n", getPlayerName(maxTDSID), playerStats[maxTDSID][2]);

    int thresholdTDS = 10;
    int thresholdYards = 1000;
    int thresholdAttempts = 100;
    int numPlayersOverThresholdTDS = numPlayerOverX(playerStats, numEntries, thresholdTDS, 2);
    int numPlayersOverThresholdYards = numPlayerOverX(playerStats, numEntries, thresholdYards, 1);
    int numPlayersOverThresholdAttempts = numPlayerOverX(playerStats, numEntries, thresholdAttempts, 0);
    printf("The number of players with more than %d TDs in 2017 is %d.\n", thresholdTDS, numPlayersOverThresholdTDS);
    printf("The number of players with more than %d yards in 2017 is %d.\n", thresholdYards, numPlayersOverThresholdYards);
    printf("The number of players with more than %d attempts in 2017 is %d.\n", thresholdAttempts, numPlayersOverThresholdAttempts);

    int thresholdYardsCarries = 100;
    int yardsPerCarryID = getMaxYardsPerCarryIndex(playerStats, numEntries, thresholdYardsCarries);
    printf("The player with the most yards per carry (>%d attempts) is %s and he had %.1lf.\n", thresholdYardsCarries,getPlayerName(yardsPerCarryID),getMaxYardsPerCarry(playerStats, yardsPerCarryID));
    thresholdYardsCarries = 50;
    yardsPerCarryID = getMaxYardsPerCarryIndex(playerStats, numEntries, thresholdYardsCarries);
    printf("The player with the most yards per carry (>%d attempts) is %s and he had %.1lf.\n", thresholdYardsCarries,getPlayerName(yardsPerCarryID),getMaxYardsPerCarry(playerStats, yardsPerCarryID));
    thresholdYardsCarries = 10;
    yardsPerCarryID = getMaxYardsPerCarryIndex(playerStats, numEntries, thresholdYardsCarries);
    printf("The player with the most yards per carry (>%d attempts) is %s and he had %.1lf.\n", thresholdYardsCarries,getPlayerName(yardsPerCarryID),getMaxYardsPerCarry(playerStats, yardsPerCarryID));



}