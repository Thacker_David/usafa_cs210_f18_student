//
// Created by C21David.Thacker on 9/19/2018.
//

#include "lab15fbFuncts.h"

int findMaxIndex(int array[][DATACOLS], int numEntries, int colToSearch){
    int bestPlayerID = 0;
    int maxScore = 0;
    for(int i = 0; i < numEntries; i++){
        if(array[i][colToSearch] >= maxScore){
            maxScore = array[i][colToSearch];
            bestPlayerID = i;
        }
    }
    return bestPlayerID;
}

int numPlayerOverX(int array[][DATACOLS], int numEntries, int threshold, int colToSearch){
    int numPlayersOver = 0;
    for(int i = 0; i < numEntries; i++){
        if(array[i][colToSearch] >= threshold){
            numPlayersOver++;
        }
    }
    return numPlayersOver;
}

int getMaxYardsPerCarryIndex(int array[][DATACOLS], int numEntires, int threshold){
    int maxYardsPerCarryID = 0;
    double maxYardsPerCarry = 0;
    for(int i = 0; i < numEntires; i++){
        if(array[i][0] >= threshold){
            if((((double)array[i][1])/array[i][0]) >= maxYardsPerCarry){
                maxYardsPerCarry = ((double) array[i][1]/array[i][0]);
                maxYardsPerCarryID = i;
            }
        }
    }
    return maxYardsPerCarryID;
}

double getMaxYardsPerCarry(int array[][DATACOLS], int iD){
    return (double) array[iD][1] / array[iD][0];
}

