//
// Created by C21David.Thacker on 9/27/2018.
//

#ifndef MYEXE_PA2FUNCS_H
#define MYEXE_PA2FUNCS_H
#include <stdio.h>
#define SIZE 100
int performOperation(char operator, int inputOne, int inputTwo);
void reverseDigits(int input);
int perfectAI(char array[]);
void getMinMax(int array[], int numValues, int* pointerOne, int* pointerTwo);
int checkPerfect(int perfectNum);
int perfectNumbers(int minRange, int maxRange, int array[]);
void fill2D(int array[][SIZE]);
double avg2D(int array[][SIZE]);

#endif //MYEXE_PA2FUNCS_H
