//
// Created by C21David.Thacker on 9/27/2018.
//

#include <memory.h>
#include <stdlib.h>
#include "pa2funcs.h"

//void reverseDigits(int input){
//    int digit;
//    do{
//        printf("%d\n", digit);
//    }
//}

int performOperation(char operator, int inputOne, int inputTwo){
    int result = 0;
    switch(operator) {
        case '+':
            result = inputOne + inputTwo;
            break;
        case '-':
            result = inputOne - inputTwo;
            break;
        case '/':
            result = inputOne / inputTwo;
            break;
        case '*':
            result = inputOne * inputTwo;
            break;
    }
    return result;
}

int perfectAI(char array[]){
    int result;

    if(array[0] == 'p'){
        result = 2;
    }
    if(array[0] == 'r'){
        result = 1;
    }
    if(array[0] == 's'){
        result = 0;
    }
    return result;
}

void getMinMax(int array[], int numValues, int* minPointer, int* maxPointer){
    int maxLocal = 0;
    int minLocal = 100000;
    for (int i = 0; i < numValues; ++i) {
        if(array[i] > maxLocal){
            maxLocal = array[i];
        }
        if(array[i] < minLocal) {
            minLocal = array[i];
        }
    }
    *minPointer = minLocal;
    *maxPointer = maxLocal;
}

int checkPerfect(int perfectNum){
    int total = 0;
    for(int i = 0; i < perfectNum; i++){
        if(perfectNum % i == 0){
            total = total + i;
        }
    }
    if(total == perfectNum){
        return 1;
    }
    else{
        return 0;
    }
}

int perfectNumbers(int minRange, int maxRange, int array[]){
    int total = 0;
    int headTo = maxRange - minRange;
    for(int i = 0; i < headTo; i++){
        if(checkPerfect(minRange) == 0){
            array[i] = minRange;
            total = total + 1;

        }
        minRange = minRange + 1;
    }
    return total;
}

void fill2D(int array[][SIZE]){
    for(int i = 0; i < SIZE; i++){
        for(int j = 0; j < SIZE; j++){
            array[i][j] = rand() % (1000 + 1);
        }
    }
}

double avg2D(int array[][SIZE]){
    double total = 0;
    for(int i = 0; i < SIZE; i++){
        for(int j = 0; j < SIZE; j++){
            total = total + array[i][j];
        }
    }
    return ((total) / (SIZE * SIZE));
}