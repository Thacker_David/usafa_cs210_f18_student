/** lab33.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Lab 33
* Documentation: DOC STATEMENT
* ===========================================================  */

#include "lab33.h"

int main() {

    // Exercise 1
    printf("\nExercise 1:\n\n");
    // The recursive function factorial() has been defined for you below. Review
    // the code and determine if it is tail recursive.
    //
    // For this exercise you will write a tail recursive version of factorial(),
    // called tail_factorial(), your function should take 2 integer parameters, the
    // first is the input value, the second is an accumulator.

    // Write your own code to test the base and recursive cases of tail_factorial()
    // to determine that it is working correctly.
    printf("%d", tail_factorial(6,1));


    // Exercise 2
    printf("\nExercise 2:\n\n");
    // The Collatz conjecture states that following a set of rules, you can reach 1
    // starting from any other number. While this conjecture has not been proven, you
    // will write a tail recursive function called collatz() that accepts two integer
    // input parameters (the first is the starting value, the second is the accumulator)
    // and returns the number of steps needed to reach 1 from the starting value.
    //
    // The rules of the Collatz conjecture are:
    //   if the current N is even, the next N = N/2
    //   if the current N is odd, the next N = 3*N + 1
    //
    // The first 10 values in the number of steps required to reach 1 are below (for testing)
    // this is the correct output for inputs of 1, 2, 3, ... 10
    //
    // 0, 1, 7, 2, 5, 8, 16, 3, 19, 6

    // Write your own code to test the base and recursive cases of collatz() to determine
    // that it is working correctly
    printf("%d", collatz(10,0));

    // Exercise 3
    printf("\nExercise 3:\n\n");
    // Download the password.zip file from the Lab 33 page in the ZyBook or from the Canvas
    // site. Extract this file to a new password directory under your Lab 33 folder. There
    // is a password hidden inside these files, write a function decode_password() which
    // searches these files recursively to find the password, starting with "_.txt".
    decode_password("_.txt");    // Your decode_password() function should accept a character array input and return void.
    // The input is the name of a text file, without the directory, so the first call would be
    // decode_password("_.txt"); Use the DIRNAME constant to add the directory to the file name
    // inside the decode_password() fuction. You might create a character array to hold the full
    // name then use sprintf() to put the directory and filename together, fullName is a character 
    // array you create, DIRNAME is a directory constant, and fname is the name you pass into the 
    // decode_password() function.
    // sprintf(fullName, "%s%s", DIRNAME, fname);
    printf("The-password-is->https://xkcd.com/710/\n");

    return 0;
}

/** ----------------------------------------------------------
 * @fn int factorial(int N)
 * @brief Recursively calculates the factorial of N
 * @param N, the input parameter
 * @return N!, the value of the factorial of N
 * ----------------------------------------------------------
 */
int tail_factorial(int N, int comp) {

    if (N == 0) {
        return comp;
    }
    else{

        return tail_factorial(N-1, N*comp);
    }
}


int collatz(int N, int step){
        if(N==1){
            return step;
        }
        if((N%2)==0){
            return collatz(N/2, step+1);
        }else{
            return collatz((3*N)+1,step+1);
        }
}

void decode_password(char fileName[]){
    FILE *fptr;
    char fullName[200];
    char check[200];
    sprintf(fullName, "%s%s", DIRNAME, fileName);
    fptr = fopen(fullName, "r");

    if (fptr == NULL) {
        printf("Could not open file\n");
    }

    while(!feof(fptr)) {
        fscanf(fptr, "%s\n", check);
        if (strlen(check) == 1 && strcmp(check, "_") != 0) {
            printf("%s", check);
        } else if(strcmp(check, "_") == 0){

        }
        else {
            decode_password(check);
        }
    }
}