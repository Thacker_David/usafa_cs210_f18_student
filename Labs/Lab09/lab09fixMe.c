/*** @file lab09fixMe.c
 *   @author CS220
 *   @brief Fixed version of the error filled file for lab09.
 */


#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// squares every value in the array
void squareIt(int array[]) {
    for (int i = 0; i < 5; i++) {
        array[i] = (int) pow(array[i],2);
    }
}

// takes square root of every value in the array
void sqrtIt(int array[]) {
    for (int i = 0; i < 5; i++) {
        array[i] = (int) sqrt(array[i]);
    }
}


int main() {
    int vals[5];

    // fill array with random #'s and print it
    for (int i = 0; i < 5; i++) {
        vals[i] = rand() % 101; //generate a random # between 0 and 100
        printf("vals[%d] = %d\n", i, vals[i]);
    }

    squareIt(vals);

    // print array after squaring it's elements
    int i = 0;
    while (i < 5) {
        printf("vals[%d] = %d\n", i, vals[i]);
        i++;
    }


    sqrtIt(vals);

    // print array after square rooting it's elements
    int j = 0;
    do {
        printf("vals[%d] = %d\n", j, vals[j]);
        j++;
    } while (j < 5);
}