//
// Created by C21David.Thacker on 9/5/2018.
//
#include <math.h>

double volumeCylinder(double cylnRadius, double cylnHeight)
{
    return M_PI * cylnRadius * cylnRadius * cylnHeight;
}

double volumeBox(double depth,double width,double height)
{
    return depth * height * width;
}

double degToRad(double degrees)
{
    return ((M_PI/180.0)*degrees);
}

