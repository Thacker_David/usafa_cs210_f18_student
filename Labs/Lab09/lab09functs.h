//
// Created by C21David.Thacker on 9/5/2018.
//

#ifndef MYEXE_LAB09FUNCTS_H
#define MYEXE_LAB09FUNCTS_H

#endif //MYEXE_LAB09FUNCTS_H


double volumeCylinder(double cylnRadius, double cylnHeight);

double volumeBox(double depth, double weidth, double height);

double degToRad(double degrees);
