//
// Created by traci.sarmiento
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct
{
    int id;
    char name[30];
    float percentage;
}Student;

int main(void)
{
    //int i;
    Student record1 = {1, "Da Student", 90.5};
    Student record2, record4;
    Student* record3;
    Student* ptr1;

    printf("Record of STUDENT1 - record1 structure \n");
    printf("  Id : %d \n  Name : %s\n  Percentage : %f\n",
           record1.id, record1.name, record1.percentage);

    // Copy an entire struct using assignment
    record2=record1;

    printf("\nRecords of STUDENT1 - Direct copy from " \
           "record1 \n");
    printf("  Id : %d \n  Name : %s\n  Percentage : %f\n",
           record2.id, record2.name, record2.percentage);

    // 2nd method to copy using memcpy function
    record3 = &record2;
    ptr1 = &record1;
    // Opportunity to discuss memcpy() function
    memcpy(record3, ptr1, sizeof(record1));

    // Treat memory like an array and access struct members
    printf("\nRecord of STUDENT1 - memory treated like an array\n");
    printf("  Id : %d \n  Name : %s\n  Percentage : %f\n",
           record3[0].id, record3[0].name, record3[0].percentage);

    // Dereference the struct pointer and access struct members
    printf("\nRecord of STUDENT1 - dereferenced struct pointer\n");
    printf("  Id : %d \n  Name : %s\n  Percentage : %f\n",
           (*record3).id, (*record3).name, (*record3).percentage);

    // Access struct members using arrow operator ("points to")
    printf("\nRecord of STUDENT1 - arrow operator\n");
    printf("  Id : %d \n  Name : %s\n  Percentage : %f\n",
           record3->id, record3->name, record3->percentage);

    /*
     * Dynamic memory allocation, structures, and structure pointers
     */

    Student* recPtr = malloc(1 * sizeof(Student));

    // Treat dynamically allocated memory as an array
    // and assign values to struct members, then print
    recPtr[0].id = 12;
    strcpy(recPtr[0].name, "Anudder Cadet");
    recPtr[0].percentage = 85.0;
    printf("\nRecord of dynamically added student - memory treated like an array\n");
    printf("  Id : %d \n  Name : %s\n  Percentage : %f\n",
           recPtr[0].id, recPtr[0].name, recPtr[0].percentage);

    // Access struct members using arrow operator ("points to")
    printf("\nRecord of dynamically added student - printed using arrow operator\n");
    printf("  Id : %d \n  Name : %s\n  Percentage : %f\n",
           recPtr->id, recPtr->name, recPtr->percentage);


    // 3rd method to copy by individual members
    printf("\nRecord of STUDENT1 - Copied individual " \
           "members from record1 \n");
    record4.id=record1.id;
    strcpy(record4.name, record1.name);
    record4.percentage = record1.percentage;

    printf("  Id : %d \n  Name : %s\n  Percentage : %f\n",
           record4.id, record4.name, record4.percentage);

    return 0;
}

