//
// Created by C21David.Thacker on 10/23/2018.
//

#ifndef MYEXE_LAB25_H
#define MYEXE_LAB25_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

typedef struct
{
    int x;
    int y;
}Point;

typedef struct
{
    char baseName[50];
    int bldgsOwned;
    int structsOwned;
    char city[50];
    char state[30];
}USAFBaseData ;

double getDistance(Point firstCoord, Point secondCoord);
void getPoints(Point* xCoord, Point* yCoord);
int getNumLines(char filename[]);
void readFile(char filename[], USAFBaseData** baseStruct, int lines);
void printData(USAFBaseData** bases, int numBases);

#endif //MYEXE_LAB25_H
