//
// Created by C21David.Thacker on 10/23/2018.
//

#include "lab25.h"

int main(){
//    int lines = getNumLines("./lab25Data.csv");
//    readFile("./lab25Data.csv", baseStruct, lines);
//    Point *point1 = malloc(sizeof(Point));
//    Point *point2 = malloc(sizeof(Point));
//
//    getPoints(point1, point2);
//    double distance = getDistance(*point1, *point2);
//    printf("%lf\n", distance);
//    char fileName[100] = "./lab25Data.csv";
//    char fileName[100] = "C:/Users/lionc/CLionProjects/usafa_cs210_f18_student/Labs/Lab25/lab25Data.csv";
    char fileName[100] = "C:/Users/C21David.Thacker/CLionProjects/usafa_cs210_f18_student/Labs/Lab25/lab25Data.csv";

//    char fileName[256] =  "../Labs/lab25/lab25Data.csv";
    int lines = getNumLines(fileName);

    USAFBaseData **baseStruct = malloc(sizeof(USAFBaseData*) * lines);
    for (int i = 0; i < lines ; ++i) {
        baseStruct[i] = malloc(sizeof(USAFBaseData));
    }
    readFile(fileName, baseStruct, lines);
    printData(baseStruct, lines);
}

int getNumLines(char filename[]) {
    char c;
    int count = 0;
    // Open the file
    FILE* fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;
}

void getPoints(Point* Point1, Point* Point2){
    printf("Please enter a x and y point\n");
    scanf("%d %d", &Point1->x, &Point1->y);
    printf("Please enter a x and y point\n");
    scanf("%d %d", &Point2->x, &Point2->y);
}

double getDistance(Point firstCoord, Point secondCoord){
    double xSolve = pow((secondCoord.x - firstCoord.x),2);
    double ySolve = pow((secondCoord.y - firstCoord.y),2);
    return sqrt(xSolve + ySolve);
}

void readFile(char filename[], USAFBaseData** baseStruct, int lines){
    FILE *out = fopen(filename, "r");
//    fseek(out, sizeof(USAFBaseData), 0);
    char temp[256];
    fgets(temp,256, out);
    int numRead = 0;

    //179TH AIRLIFT WING OHIO ANG,32,45,Mansfield,Ohio
    while(numRead < lines && !feof(out)){
        fscanf(out, "%[^,], %d, %d, %[^,], %[^\n]",  baseStruct[numRead]->baseName, &baseStruct[numRead]->bldgsOwned, &baseStruct[numRead]->structsOwned,
            baseStruct[numRead]->city, baseStruct[numRead]->state);

        numRead++;
    }
    printf("%d\n", numRead);
    if(fclose(out) != 0) {
    printf("Failure to chooch\n");
    }

}

void printData(USAFBaseData** bases, int numBases){
    for(int i = 0; i < numBases; i++) {
        printf("%s, %d, %d, %s, %s\n", bases[i]->baseName, bases[i]->bldgsOwned, bases[i]->structsOwned,
               bases[i]->city, bases[i]->state);
    }
}
