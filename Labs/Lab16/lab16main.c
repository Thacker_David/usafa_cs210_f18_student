//
// Created by C21David.Thacker on 9/25/2018.
//

#include "lab16functs.h"

int main(){
    double resVals[NUM_RES];
    double circVolt = 12;
    double vDrop[NUM_RES];
    double currentVal = 0;

    captureOhms(resVals);
    calculateCurrent(circVolt, resVals,&currentVal);
    voltageDrop(vDrop, resVals, currentVal);
    printDrop(vDrop);
}