//
// Created by C21David.Thacker on 9/25/2018.
//

#include "lab16functs.h"

void captureOhms(double resVals[]){
    for(int i = 0; i < NUM_RES; i++){
        printf("Please enter a resistor value.\n");
        scanf("%lf", &resVals[i]);
    }
}

void calculateCurrent(double circVolt,double resVals[], double* currentVal){
    double totalRes = 0;
    for(int i = 0; i < NUM_RES; i++) {
        totalRes = totalRes + resVals[i];
    }
    *currentVal = circVolt / totalRes;
}

void voltageDrop(double vDrop[], double resVals[], double currentVal){
    for (int i = 0; i < NUM_RES; ++i) {
        vDrop[i] = currentVal * resVals[i];
    }
}

void printDrop(double vDrop[]){
    for (int i = 0; i < NUM_RES; ++i) {
        printf("%d) %.1lf V\n", i + 1 , vDrop[i]);
    }
}
