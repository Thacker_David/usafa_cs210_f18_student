//
// Created by C21David.Thacker on 9/25/2018.
//

#ifndef MYEXE_LAB16_H
#define MYEXE_LAB16_H

#include <stdlib.h>
#include <stdio.h>
#define  NUM_RES 5
void captureOhms(double resVals[]);
void calculateCurrent(double circVolt,double resVals[], double* currentVal);
void voltageDrop(double vDrop[], double resVals[], double currentVal);
void printDrop(double vDrop[]);
#endif //MYEXE_LAB16_H
