//
// Created by C21David.Thacker on 9/17/2018.
//

#ifndef MYEXE_LAB13_H
#define MYEXE_LAB13_H
#include <stdio.h>

void swapPassByValue(int input1, int input2);
void swapPassByReference(int* input1, int* input2);
int pointerSum(int* toSumOne, int* toSumTwo);
void findFactorial(int factorio, long* facotrial);
#endif //MYEXE_LAB13_H
