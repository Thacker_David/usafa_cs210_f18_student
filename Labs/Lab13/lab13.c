//
// Created by C21David.Thacker on 9/17/2018.
//


#include "lab13.h"
int main() {
    int input1;
    int input2;
    printf("Please enter your class year:\n");
    scanf("%d", &input1);
    printf("Please enter your favorite integer:\n");
    scanf("%d", &input2);
    swapPassByValue(input1,input2);
    printf("aNum = %d, ", input1);
    printf("bNum = %d\n", input2);
    swapPassByReference(&input1, &input2);
    printf("aNum = %d, ", input1);
    printf("bNum = %d\n", input2);
    printf("Please enter two numbers separated by a space\n");
    int toSumOne;
    int toSumTwo;
    scanf("%d %d", &toSumOne, &toSumTwo);
    printf("The sum of %d and %d = %d\n", toSumOne, toSumTwo, pointerSum(&toSumOne, &toSumTwo));
    printf("Please enter a integer\n");
    int factorio;
    long factorial = 1;
    scanf("%d", &factorio);
    findFactorial(factorio, &factorial);
    printf("The factorial of %d is %ld", factorio, factorial);
}

void swapPassByValue(int input1, int input2){
    int temp = input1;
    input1 = input2;
    input2 = temp;
}

void swapPassByReference(int* input1, int* input2){
    int temp = *input1;
    *input1 = *input2;
    *input2 = temp;
}

int pointerSum(int* toSumOne, int* toSumTwo){
    return *toSumOne + *toSumTwo;
}

void findFactorial(int factorio, long* factorial){
    for(int i = 1; i <= factorio;i++){
        *factorial *= i;
    }
}



