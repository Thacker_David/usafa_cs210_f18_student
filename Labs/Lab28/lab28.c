
/** lab28.c
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Progamming Assessment 3 Practice
* Documentation: DOC STATEMENT
* ===========================================================  */

#include "lab28.h"

int main() {
//    char fileName[100] = "C:/Users/C21David.Thacker/CLionProjects/usafa_cs210_f18_student/Labs/Lab28/O1_BAH_2018.txt";
    char fileName[100] = "C:/Users/lionc/CLionProjects/usafa_cs210_f18_student/Labs/Lab28/O1_BAH_2018.txt";
int lines = getNumLines(fileName);

    // 2) Dynamically allocate memory to store the BAH data in the text file.
    BAHRecord* bahData = malloc(lines * sizeof(BAHRecord));

    // 3) Create a user-defined function called readFile, which
    //   requires the filename, memory passed by reference, and
    //   the number of records to be passed as parameters (in that order).
    //   It returns the actual number of records read.
    int numRead = readFile(fileName, bahData, lines);
    printf("%d\n", numRead);


    // 4) Create a user-defined function called greatestRate to determine
    //   the city, state, and amount of the highest with-dependent rate.
    //   This function accepts the stored data, the number of records, and
    //   a pointer to a char array to capture the city and state (in that order).  
    //   It returns the greatest rate.
    char locationPtr[50];
    double greatestBAH = greatestRate(bahData, numRead, locationPtr);
    printf("%lf\n", greatestBAH);
    printf("%s\n", locationPtr);

    // 5) Create a user-defined function called leastRate to determine
    //   the city, state, and amount of the least without-dependent rate.
    //   This function accepts the stored data, the number of records, and
    //   a pointer to a char array to capture the city and state (in that order).  
    //   It returns the least rate.

    char locationPtr1[50];
    double leastBAH = leastRate(bahData, numRead, locationPtr1);
    printf("%lf\n", leastBAH);
    printf("%s\n", locationPtr1);


    // 6) Create a user-defined function called numStates to determine
    //   the number of states represented in the file.  This function accepts the
    //   stored data and the number of records (in that order).  It returns the 
    //   number of states.
    //   Assume that the entries are sorted alphabetically by state.

    int numUniqueStates = numStates(bahData, numRead);
    printf("%d\n", numUniqueStates);

    // 7) Create a user-defined function called highRates to determine
    //   the number of cities with BAH rates (with or without dependent)
    //   above $1,000.  This function accepts the stored data and the number
    //   of records (in that order).  It returns the number of cities.
    int numCities = highRates(bahData, numRead);
    printf("%d\n", numCities);


    // 8) Create a user-defined function called longestCity to determine
    //   the city with the longest name.  This function accepts the stored data 
    //   the number of records, and a pointer to a character array to capture the city.
    //   It returns a pointer to the character array storing the city name as a string.
    char locationPtr2[50];
    char* locationPtr3 = longestCity(bahData, numRead, locationPtr2);
    printf("%s\n", locationPtr3);

    // 9) Create a user-defined function called updateState to update the abbreviation
    //   for Colorado from "CO" to "Colorado" for each city/state pair.  Once these
    //   updates are complete, then write all the data to a file called "outputFile.txt".
    //   This function accepts the stored data and the number of records (in that order). 
    //   It does not have a return value.
    updateState(bahData, numRead);



    return 0;
}

int getNumLines(char filename[]) {
    char c;
    int count = 0;
    // Open the file
    FILE *fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;
}

int readFile(char filename[], BAHRecord* baseStruct, int lines) {
    FILE *out = fopen(filename, "r");
    int numRead = 0;

    while (numRead < lines&& !feof(out)) {
        fscanf(out, "%[^,], %[^,], %lf, %lf%*c", baseStruct[numRead].state, baseStruct[numRead].city, &baseStruct[numRead].withDep,
               &baseStruct[numRead].withoutDep);

        printf("%s, %s, %lf, %lf\n", baseStruct[numRead].state, baseStruct[numRead].city, baseStruct[numRead].withDep,
               baseStruct[numRead].withoutDep);

        numRead++;
    }
    printf("%d\n", numRead);
    if (fclose(out) != 0) {
        printf("Failure to chooch\n");
    }
    return numRead;
}

double greatestRate(BAHRecord* baseStruct, int numRecords, char locationPtr[50]){
    double max = 0;
    int maxI = 0;
    char src[50], dst[50];
    for (int i = 0; i < numRecords; ++i) {
        if(baseStruct[i].withoutDep > max){
            max = baseStruct[i].withoutDep;
            maxI = i;
        }
    }
    strcpy(src, baseStruct[maxI].city);
    strcpy(dst, baseStruct[maxI].state);
    strcat(src, " ");
    strcat(src, dst);
    strcat(locationPtr, src);
    return max;
}

double leastRate(BAHRecord* baseStruct, int numRecords, char locationPtr[50]){
    double min = 100000;
    int maxI = 0;
    char src[50], dst[50];
    for (int i = 0; i < numRecords; ++i) {
        if(baseStruct[i].withoutDep < min){
            min = baseStruct[i].withoutDep;
            maxI = i;
        }
    }
    strcpy(src, baseStruct[maxI].city);
    strcpy(dst, baseStruct[maxI].state);
    strcat(src, " ");
    strcat(src, dst);
    strcat(locationPtr, src);
    return min;
}

int numStates(BAHRecord* baseStruct, int numRecords){
    char state[9] = "AK";
    int numStates = 0;

    for (int i = 0; i < numRecords; ++i) {
        if(strcmp(baseStruct[i].state, state) != 0){
        strncpy(state, baseStruct[i].state, 2);
        numStates++;
        }
    }
    return numStates+1;
}

int highRates(BAHRecord* baseStruct, int numRecords){
    int numCities = 0;
    for (int i = 0; i < numRecords; ++i) {
        if(baseStruct[i].withoutDep > 1000.00 || baseStruct[i].withDep > 1000.00){
            numCities++;
        }
    }
    return numCities;
}

char* longestCity(BAHRecord* baseStruct, int numRecords, char locationPtr[50]){
    int maxI =0;
    int longestStr = 0;
    for (int i = 0; i < numRecords; ++i) {
        if(strlen(baseStruct[i].city) < longestStr){
            longestStr = strlen(baseStruct[i].city);
            maxI = i;
        }
    }
    strcat(locationPtr, baseStruct[maxI].city);
    return locationPtr;
}

void updateState(BAHRecord* baseStruct, int numRecords){
    char state[9] = "CO";

    for (int i = 0; i < numRecords; ++i) {
        if(strcmp(baseStruct[i].state, state) == 0){
            strncpy(baseStruct[i].state, "Colorado", 8);


        }
        char fileName[100] = "C:/Users/lionc/CLionProjects/usafa_cs210_f18_student/Labs/Lab28/outputFile.txt";
        FILE *out;
        out = fopen(fileName,"w");
        if (out == NULL)
        {
            printf("Could not open file %s\n", fileName);
        }
//        fwrite(baseStruct, sizeof(BAHRecord), numRecords, out);
    }
}


