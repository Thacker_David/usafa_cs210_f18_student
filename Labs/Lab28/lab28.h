/** lab28.h
* ===========================================================
* Name: FIRST LAST, DATE
* Section: SECTION
* Project: Programming Assessment 3 Practice
* Documentation: DOC STATEMENT
* ===========================================================  */

#ifndef MYEXE_LAB28_H
#define MYEXE_LAB28_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct {
    char state[9];
    char city[20];
    double withDep;
    double withoutDep;
}BAHRecord;

int getNumLines(char filename[]);
int readFile(char filename[], BAHRecord* baseStruct, int lines);
double greatestRate(BAHRecord* baseStruct, int numRecords, char locationPtr[50]);
double leastRate(BAHRecord* baseStruct, int numRecords, char locationPtr[50]);
int numStates(BAHRecord* baseStruct, int numRecords);
int highRates(BAHRecord* baseStruct, int numRecords);
char* longestCity(BAHRecord* baseStruct, int numRecords, char locationPtr[50]);
void updateState(BAHRecord* baseStruct, int numRecords);






#endif //MYEXE_LAB28_H
