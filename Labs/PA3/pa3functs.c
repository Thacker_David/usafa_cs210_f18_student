//
// Name: David Thacker
//

#include <ctype.h>
#include "pa3functs.h"

int getNumLines(char filename[]) {
    char c;
    int count = 0;
    // Open the file
    FILE *fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;
}

void readFile(char filename[], AirCraftType *dataPtr, int recs) {
    FILE *out = fopen(filename, "r+");
    int numRead = 0;

    while (numRead < recs && !feof(out)) {
        fscanf(out, "%[^,], %[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%d,%d%*c%*c", dataPtr[numRead].operAirline,
               dataPtr[numRead].geoRegion, dataPtr[numRead].acType,
               dataPtr[numRead].acBody, dataPtr[numRead].acManu, dataPtr[numRead].acMod, dataPtr[numRead].acVer,
               &dataPtr[numRead].landCount, &dataPtr[numRead].weight);

        numRead++;
    }
//    for (int i = 0; i < recs; ++i) {
//        printf("%s,%s,%s,%s,%s,%s,%s,%d,%d\n", dataPtr[i].operAirline, dataPtr[i].geoRegion, dataPtr[i].acType,
//               dataPtr[i].acBody,  dataPtr[i].acManu, dataPtr[i].acMod, dataPtr[i].acVer,  dataPtr[i].landCount,  dataPtr[i].weight);
//
//    }
    printf("%d\n", numRead);
    if (fclose(out) != 0) {
        printf("Failure to chooch\n");
    }
}

int findIndexMostWeight(AirCraftType *dataPtr, int recs) {
    int max = 0;
    int maxI = 0;
    for (int i = 0; i < recs; ++i) {
        if (dataPtr[i].weight > max) {
            max = dataPtr[i].weight;
            maxI = i;
        }
    }

    return maxI;
}

AirCraftType *findPtrMostWeight(AirCraftType *dataPtr, int recs) {
    int max = 0;
    int maxI = 0;
    for (int i = 0; i < recs; ++i) {
        if (dataPtr[i].weight > max) {
            max = dataPtr[i].weight;
            maxI = i;
        }
    }

    return &dataPtr[maxI];
}

int countRegion(AirCraftType *dataPtr, int recs, char *region) {
    int numRegion = 0;
    for (int i = 0; i < recs; ++i) {
        if (strcmp(dataPtr[i].geoRegion, region) == 0) {
            numRegion++;
        }
    }
    return numRegion;
}

void updateType(AirCraftType *dataPtr, int recs) {
    for (int i = 0; i < recs; ++i) {
        if (strcmp(dataPtr[i].acType, "Passenger") == 0) {
            strncpy(dataPtr[i].acType, "PA\0", 3);

        }
    }
}

void writeNewFile(AirCraftType *dataPtr, int recs) {
    char fileName[100] = "C:/Users/C21David.Thacker/CLionProjects/usafa_cs210_f18_student/Labs/PA3/outputFile.txt";
    FILE *out;
    out = fopen(fileName, "w+");
    if (out == NULL) {
        printf("Could not open file %s\n", fileName);
    }

    fwrite(dataPtr, sizeof(AirCraftType), recs, out);

}

int countLetter(AirCraftType* dataPtr, int recs, char letter){
    char temporary[50];
    char tempString[2];
    int sameLetter = 0;

    for (int i = 0; i < recs; ++i) {
        for (int j = 0; j < strlen(dataPtr[i].acManu); ++j) {
            if(strcmp(tempString, &letter) == 0){
                sameLetter++;
            }
            tempString[1] = toupper(tempString[1]);
            if(strcmp(tempString, &letter) == 0){
                sameLetter++;
            }
        }
    }
    return sameLetter;
}

int uniqueManufact(AirCraftType *dataPtr, int recs) {

    char** manuArray = malloc(sizeof(dataPtr->acManu) * recs);
    for (int k = 0; k < recs; ++k) {
        *manuArray = malloc(sizeof(dataPtr->acManu));
    }
    int numRecords = 1;
    strcat(manuArray[0], dataPtr[0].acManu);
    for (int i = 0; i < recs; ++i) {
        for (int j = 0; j < numRecords; ++j) {
            if (strcmp(dataPtr[i].acManu, manuArray[j]) != 0) {
                strcat(manuArray[j], dataPtr[i].acManu);
                numRecords++;
            }

        }
    }
    return numRecords;
}