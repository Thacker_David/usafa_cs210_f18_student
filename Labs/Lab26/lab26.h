#ifndef MYEXE_LAB26_H
#define MYEXE_LAB26_H

#include "vector.h"
#include <stdlib.h>


int getNumLines(char filename[]);
void readFile(char filename[], vector* vectorPtr, int records);

#endif //MYEXE_LAB26_H