
#include "lab26.h"

int main(void)
{
//    char fileName[100] = "C:/Users/C21David.Thacker/CLionProjects/usafa_cs210_f18_student/Labs/Lab26/lab26Data.csv";
    char fileName[100] = "./lab26Data.csv";

    // With the functions provided in the Vector ADT,
   //   create a correctly sized vector to read-in all
   //   data from the lab26Data.csv file
   vector* vPtr;
   vPtr = malloc(sizeof(vector));

   // Utilize your readFile() function from Lab 25
   //   to populate the vector -- you will need to make changes
   //   for this function to perform correctly
   int lines = getNumLines(fileName);
//   printf("%d\n", lines);
   vector_create(vPtr, lines);

    readFile(fileName, vPtr, lines);
   // With the functions provided in the Vector ADT,
   //   print the number of vector elements
   printf("%d\n", vector_size(vPtr));
    
   // With the functions provided in the Vector ADT,
   //   insert a 12 at index 139
//   printf("%d",*vector_at(vPtr, 139));
    vector_insert(vPtr, 139, 12);
    

   // With the functions provided in the Vector ADT,
   //   print the number of vector elements
   printf("%d\n", vector_size(vPtr));


    // With the functions provided in the Vector ADT,
   //   print the element at index 999
       printf("%d\n",*vector_at(vPtr, 999));



    // With the functions provided in the Vector ADT,
   //   erase the element at index 999
   vector_erase(vPtr, 999);
    

   // With the functions provided in the Vector ADT,
   //   print the element at index 999

    printf("%d\n",*vector_at(vPtr, 999));


    // With the functions provided in the Vector ADT,
   //   print the number of vector elements
    printf("%d\n", vector_size(vPtr));



    // Using the Vector ADT provided function
    //   vector_push_back to insert the integer 222
    vector_push_back(vPtr, 222);
   

   // With the functions provided in the Vector ADT,
   //   print the number of vector elements
    printf("%d\n", vector_size(vPtr));



    // With the functions provided in the Vector ADT,
   //   destroy the vector
   vector_destroy(vPtr);
  
}

int getNumLines(char filename[]) {
    char c;
    int count = 0;
    // Open the file
    FILE* fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL)
    {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count;
}

void readFile(char filename[], vector* vectorPtr, int records){
    FILE *out = fopen(filename, "r+");

    int numRead = 0;

    while(numRead < records && !feof(out)){
        fscanf(out, "%d,", &vectorPtr->elements[numRead]);
        numRead++;
    }
//    printf("%d\n", numRead);

    if(fclose(out) != 0) {
        printf("Failure to chooch\n");
    }

}