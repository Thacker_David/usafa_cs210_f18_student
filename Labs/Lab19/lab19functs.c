//
// Created by C21David.Thacker on 10/3/2018.
//
#include "lab19functs.h"

void replStr(char str[], char aChar, char bChar){
    for(int i = 0; i < strlen(str); i++){
        if(str[i] == aChar){
            str[i] = bChar;
        }
    }
}
//This function replaces all occurrences of the characters contained in searchChars (an array of multiple search characters) in the string, str with the single char, rChar .
void replMultiChar(char str[], char searchChars[], char rChar ){
    for(int i = 0; i < strlen(searchChars); i++){
        for(int j = 0; j < strlen(str); j++){
            if(str[j] == searchChars[i]){
                str[j] = rChar;
            }
        }
    }
}

int findLocations(char str[], int locations[], char searchChar) {
    int count = 0;
    for (int i = 0; i < strlen(str); i++) {
        if (str[i] == searchChar) {
            count++;
            locations[count] = i;
        }
    }
    return count;
}

