//
// Created by C21David.Thacker on 10/3/2018.
//

#ifndef MYEXE_LAB19FUNCTS_H
#define MYEXE_LAB19FUNCTS_H
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define MAX_STR_LEN 81
void replStr(char str[], char aChar, char bChar);
//This function replaces all occurrences of the characters contained in searchChars (an array of multiple search characters) in the string, str with the single char, rChar .
void replMultiChar(char str[], char searchChars[], char rChar );
int findLocations(char str[], int locations[], char searchChar);
int countSeqStr(char str[], char searchStr[]);
#endif //MYEXE_LAB19FUNCTS_H
