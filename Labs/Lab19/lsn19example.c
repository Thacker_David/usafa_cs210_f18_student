//
// Created by Troy.Weingart on 9/28/2018.
//
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_LEN 82
int countLetter(char str[], char ltr);

int main() {
    char buf[MAX_LEN]; // a buffer to hold user input
    char letter = '\0';

    // Ask the user for a letter
    printf("Enter the letter to search for: ");
    scanf(" %c",&letter);

    printf("The number of %c's in the string is %d.\n", letter,
            countLetter(buf,letter));
}

int countLetter(char str[], char ltr) {
    int count = 0;

    // ensure we get a letter from the user
    if (!isalpha((int) ltr)) {
        return -1;
    }

    // process the string using a for loop
    for (int i = 0; i < strlen(str); i++) {
        if (tolower((int) ltr) == tolower((int) str[i])) {
            count++;
        }
    }
    return count;
}