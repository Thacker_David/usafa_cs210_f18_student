//
// Created by C21David.Thacker on 10/3/2018.
//
#include "lab19functs.h"

int main(){
    printf("Function - 1\n");
    printf("Enter a string (<= 80 chars): ");
    char firstInput[MAX_STR_LEN];
    fgets(firstInput, MAX_STR_LEN, stdin);
    printf("\n");
    printf("Enter a search character followed by replace character:");
    char firstChar;
    char secondChar;
    scanf("%c", &firstChar);
    scanf(" %c", &secondChar);
    replStr(firstInput, firstChar, secondChar);
    printf("New string (between arrows): -->%s<--\n", firstInput);
    char ch = '\0';
    while ((ch = getchar()) != '\n' && ch != EOF);

    printf("Function - 2\n");
    printf("Enter a string (<= 80 chars): ");
    char secondInput[MAX_STR_LEN];
    fgets(secondInput, MAX_STR_LEN, stdin);
    char searchString[MAX_STR_LEN];
    fgets(searchString, MAX_STR_LEN, stdin);
    char replaceCharacter;
    scanf("%c", &replaceCharacter);
    replMultiChar(secondInput, searchString, replaceCharacter);
    printf("New string (between arrows): -->%s<--\n", secondInput);
    while ((ch = getchar()) != '\n' && ch != EOF);

    printf("Function - 3\n");
    printf("Enter a string (<= 80 chars): ");
    char thirdInput[MAX_STR_LEN];
    int locations[MAX_STR_LEN];
    fgets(thirdInput, MAX_STR_LEN, stdin);
    printf("\n");
    printf("Enter a character whose locations you wish to find: ");
    char charLocation;
    scanf("%c", &charLocation);
    int count = findLocations(thirdInput, locations, charLocation);
    printf("There are %d occurrences of %c", count, charLocation);
    for(int i = 0; i < count; i++){
        printf("%c found at location %d", charLocation, locations[i]);
    }








}