/*** @file lab10main.c
*    @author David Thacker
*    @brief Lab 10 main
*/


#include "lab10functs.h"

int main(){
    //isFirstHalfAlphaSection
    printf("Enter a single character to pass to isFirstHalfAlpha():\n");
    char alphaInput;
    scanf("%c", &alphaInput);
    bool isBack = isFirstHalfAlpha(alphaInput);
    if(isBack == true) {
        printf("A %c is in the first half of the alphabet.\n", alphaInput);
    }
    else {
        printf("A %c is NOT in the first half of the alphabet.\n", alphaInput);
    }

    //typeOfChar section
    printf("Enter a single character to pass to typeOfChar():\n");
    char secondInput;
    scanf(" %c", &secondInput);
    int gotBack = typeOfChar(secondInput);
    if(gotBack == 0){
        printf("A %c is a vowel.\n", secondInput);
    }
    else if(gotBack == 1){
        printf("A %c is a consonant.\n", secondInput);
    }
    else{
        printf("A %c is neither a vowel nor a consonant.\n", secondInput);
    }

    //magicChar section
    printf("Enter a single character to pass to isMagicChar():\n");
    char magicCharInput;
    scanf(" %c", &magicCharInput);
    gotBack = isMagicChar(magicCharInput);
    if(gotBack == true){
        printf("A %c is the magic character %c.\n", magicCharInput, MAGIC_CHAR);
    }
    else{
        printf("A %c is NOT the magic character %c.\n", magicCharInput, MAGIC_CHAR);
    }

}
