//
// Created by C21David.Thacker on 9/7/2018.
//

#include <ctype.h>
#include <stdbool.h>
#include "lab10functs.h"

bool isFirstHalfAlpha(char alphaInput)
{
    alphaInput = tolower(alphaInput);
    int a = alphaInput;
    return a >= 97 && a <= 109 ? true : false;
}

int typeOfChar(char secondInput)
{
    int whatTo;
    if(isalpha(secondInput)) {
        secondInput = tolower(secondInput);
        switch (secondInput) {
            case 'a':
                whatTo = 0;
                break;
            case 'e':
                whatTo = 0;
                break;
            case 'i':
                whatTo = 0;
                break;
            case 'o':
                whatTo = 0;
                break;
            case 'u':
                whatTo = 0;
                break;
            default:
                whatTo = 1;
                break;
        }
    }
    else {
        whatTo = -1;
    }
    return whatTo;
}

int isMagicChar(char magicCharInput) {
    return magicCharInput == MAGIC_CHAR ? 1 : 0;
}
