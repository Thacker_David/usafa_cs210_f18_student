//
// Created by C21David.Thacker on 9/7/2018.
//

#ifndef MYEXE_LAB10FUNCTS_H
#define MYEXE_LAB10FUNCTS_H
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#define MAGIC_CHAR '@'

bool isFirstHalfAlpha(char alphaInput);
int typeOfChar(char secondInput);
int isMagicChar(char magicCharInput);

#endif //MYEXE_LAB10FUNCTS_H
