//
// Created by C21David.Thacker on 10/17/2018.
//

#ifndef MYEXE_LAB23_H
#define MYEXE_LAB23_H

#include <stdio.h>
#include <memory.h>
#include <errno.h>

#include <stdlib.h>

typedef struct CadetInfoStruct {
    char name[50];
    int age;
    int squad;
    int year;
} CadetInfoStructType;

int getNumRecs(char dataFile[]);
void getDataText(CadetInfoStructType cadetRecords[], int numRecs, char dataFile[]);

#endif //MYEXE_LAB23_H
