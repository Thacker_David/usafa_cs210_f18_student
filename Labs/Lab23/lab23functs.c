//
// Created by C21David.Thacker on 10/17/2018.
//


#include "lab23functs.h"

/** ----------------------------------------------------------
 * @fn int getNumRecs(char dataFile[])
 * @brief Reads the number of records from the data file
 * @param dataFile is a string that indicates the path to and filename of the datafile
 * @return number of records in the file or -1 on error
 * ----------------------------------------------------------
 */

int getNumRecs(char dataFile[]){
    FILE *out = fopen(dataFile, "r+");
    if (out == NULL) {
        printf("Error creating data file: %s.\n", strerror(errno));
        exit(-1);
    }
    int numElements = 0;
    fscanf(out, "%d", &numElements);
    fclose(out);

    return numElements;
}

/** ----------------------------------------------------------
 * @fn void getDataText(CadetInfoStructType cadetRecords[], int numRecs, char dataFile[])
 * @brief Reads CadetInfoStructType  records from a text file
 * @param cadetRecords is the array of cadet records
 * @param numRecs is the number of records in the file
 * @param dataFile is a string that indicates the path to and filename of the datafile
 * ----------------------------------------------------------
 */

void getDataText(CadetInfoStructType cadetRecords[], int numRecs, char dataFile[]){
    FILE *out = fopen(dataFile, "r+");
    char firstName[10];
    char lastName[20];
    int numRead = 0;

    fseek(out, sizeof(int), 0);

    while (numRead < numRecs && !feof(out)) {
        fscanf(out, "%s %s %d %d %d", firstName, lastName, &cadetRecords[numRead].age,
               &cadetRecords[numRead].squad, &cadetRecords[numRead].year);
        strcat(firstName, " ");
        strcpy(cadetRecords[numRead].name, strcat(firstName, lastName));
        numRead++;
    }
    fclose(out);

}