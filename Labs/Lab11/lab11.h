//
// Created by C21David.Thacker on 9/11/2018.
//

#ifndef MYEXE_LAB11_H
#define MYEXE_LAB11_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int numFactorial(int factoralInput);
int isPrime(int primeInput);
void floydsTriangle(int floydInput);
#endif //MYEXE_LAB11_H
