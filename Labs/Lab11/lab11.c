//
// Created by C21David.Thacker on 9/11/2018.
//

#include "lab11.h"

int main(){
    int factoralInput;
    printf("Please enter a number to factoralize.\n");
    scanf("%d", &factoralInput);
    printf("The factorial of %d is %d.\"\n", factoralInput , numFactorial(factoralInput));
    int primeInput;
    printf("Please enter a number to be primed.\n");
    scanf("%d", &primeInput);
    printf(isPrime(primeInput) == 1 ? "%d is a prime number.\n" : "%d is not a prime number.\n", primeInput);
    int flyodInput;
    printf("Please enter a number to be floyd'ed.\n");
    scanf("%d", &flyodInput);
    floydsTriangle(flyodInput);
}

int numFactorial(int factoralInput){
    int factoralResult = factoralInput;
    for(int i = 1; i < factoralInput; i++){
        factoralResult *= i;
    }
    return factoralResult;
}

int isPrime(int primeInput) {
    for (int i = 2; i < primeInput; i++) {
        if ((primeInput % i) == 0) {
            false;
            return 0;
        }
    }
    return 1;
}

void floydsTriangle(int floydInput){
    for (int i = 0; i <= floydInput; i++) {

        for (int j = 0; j < i; j++) {
            printf(i % 2 == 0 ? j % 2 == 0 ? "0" : "1" : j % 2 == 0 ? "1" : "0");
        }
        printf("\n");
    }
}

