//
// Created by C21David.Thacker on 9/13/2018.
//

#ifndef MYEXE_LAB12_H
#define MYEXE_LAB12_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int isArmstrong(int armstrongInput);

int findGCD(int firstInput, int secondInput);

void printRange(int start, int end);

#endif //MYEXE_LAB12_H
