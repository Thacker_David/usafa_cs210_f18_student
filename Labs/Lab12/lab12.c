/** lab12.c
* ==============================================================
* Name: David Thacker, 090818
* Section: T3-4
* Project: lab12
* Documentation Statement: Help on the armstrong function to figure out what I was doing and how to test for it.
* ==============================================================
* Instructions:
 * Complete and submit lab12 as outlined in the writeup.
*/

#include "lab12.h"

int main() {
    int armstrongInput;
    printf("Please enter a number:\n");
    scanf("%d", &armstrongInput);
    if (isArmstrong(armstrongInput) == 0) {
        printf("%d is an Armstrong number.\n", armstrongInput);
    } else {
        printf("%d is not an Armstrong number.\n", armstrongInput);
    }
    printf("Please enter two numbers\n");
    int firstInput;
    int secondInput;
    scanf("%d", &firstInput);
    scanf("%d", &secondInput);
    int whatGot = findGCD(firstInput, secondInput);
    printf("GCD = %d", whatGot);
    int start;
    int end;
    scanf("%d", &start);
    scanf(" %d", &end);
    printRange(start, end);
}

int isArmstrong(int armstrongInput) {
    int temp = 0;
    int remainder = 0;
    double result = 0;

    temp = armstrongInput;
    while (temp != 0) {
        remainder = temp % 10;
        result = result + pow(remainder, 3);
        temp = temp / 10;
    }

    if (result == armstrongInput) {
        return 1;
    } else {
        return 0;
    }
}

int findGCD(int firstInput, int secondInput) {
    int i = 1;
    int greatestOfCommonDemoniatiors;
    while (i <= firstInput && i <= secondInput) {
        if (firstInput % i == 0 && secondInput % i == 0) {
            greatestOfCommonDemoniatiors = i;
        }
        i++;
    }
    return greatestOfCommonDemoniatiors;
}

void printRange(int start, int end) {
    int i = start;
    while (i <= end) {
        printf("%d\n", i);
        i++;
    }
}
