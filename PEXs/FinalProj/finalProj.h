/** finalProj.h
* ==============================================================
* Name: David Thacker, 291018
* Section: T3-4
* Project: Final Project - Game of Life
* Documentation Statement: None
* ==============================================================
* Instructions:
 * Design, implement, and test, a emulation of Conway's game of life, a simulation of cells on a grid
 * Adhere to all programming standards and the finalProj rubric
*/
#ifndef MYEXE_FINALPROJ_H
#define MYEXE_FINALPROJ_H

#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>
#include <locale.h>
#include <wchar.h>
#include <stdbool.h>
#include <time.h>
#include <locale.h>
#include <wchar.h>

#define MAX_VALUEY 100
#define MAX_VALUE 200
#define MAX_VALUESTAR 250
#define MIN_VALUESTAR 5
#define ALIVEUNICODE "\u25AE"
#define DEADUNICODE "\u25AF"
#define MIN_VALUE 10
#define MAX_STARS 10
#define DELAY 50
//#define FILENAME "C:/Users/lionc/CLionProjects/usafa_cs210_f18_student/PEXs/FinalProj/saveFile.txt"

#define FILENAME "../PEXs/FinalProj/saveFile.txt"

typedef struct {
    int position;
    int status;
    int *partnerOne;
    int *partnerTwo;
    int *partnerThree;
    int *partnerFour;
    int *partnerFive;
    int *partnerSix;
    int *partnerSeven;
    int *partnerEight;
    int xPos;
    int yPos;
} cell;

int unicodeDatabase[100];


/**   ----------------------------------------------------------
 * This function contains the code needed to generate the alive or dead status of a individual cell
 * The rand functions are affected by the seed passed in, so that the user can have either the same experiance
 * or a different experience every time.
 * @param
 * This function takes 4 inputs, a cellStruct of type cell, which contains the settings for each cell
 * The x and y size of the window
 * The seed passed in either by the user, from the save, or from the time on the machine
 * @return
 * This function returns nothing.
 */
void populate(cell **cellStruct, int x, int y, int seed);

/**   ----------------------------------------------------------
 * This function contains the code needed to assign each individual cell its partners. The partners are defined as
 * any cell within the 3x3 radius around the targeted individual cell. Partner cells are passed as the pointer to the
 * status of a partner cell. Cells on the edge or without a full 8 set of partners are assigned a partner who's status
 * is always dead.
 * @param
 * This function takes 4 inputs, a cellStruct of type cell, which contains the settings for each cell
 * The x and y size of the window
 * @return
 * This function returns nothing.
 */
void assignPartners(cell **cellStruct, int x, int y);

/**   ----------------------------------------------------------
 * This function contains the code needed to load a scenario from a save or scenario file
 * @param
 * This function takes no parameters
 * @return
 * This function a 1 should the function execute correctly
 */
int loadScenario();

/**   ----------------------------------------------------------
 * This function contains the code needed to determine the status of the surrounding cells and apply the rules to the cell
 * The rules are:
 *  Any live cell with fewer than two live neighbors dies, as if by underpopulation.
    Any live cell with two or three live neighbors lives on to the next generation.
    Any live cell with more than three live neighbors dies, as if by overpopulation.
    Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
 * @param
 * This function takes 2 inputs, a cellStruct of type cell, which contains the settings for each cell
 * The length of the cell which is the x * y of the window
 * @return
 * This function returns nothing.
 */
void deadOrAlive(cell **cellStruct, int length);

/**   ----------------------------------------------------------
 * This function contains the code needed to draw on the screen the position and status of each cell
 * What is drawn is determined by a global unicode in the finalProj.h file
 * @param
 * This function takes 3 inputs, a cellStruct of type cell
 * The x and y size of the window
 * @return
 * This function returns nothing.
 */
void draw(cell **cellStruct, int x, int y);

/**   ----------------------------------------------------------
 * This function contains the code needed to load a scenario from a save or scenario file
 * @param
 * This function takes no parameters
 * @return
 * This function a 1 should the function execute correctly
 */
int loadSave(cell **cellStruct);

/**   ----------------------------------------------------------
 * This function contains the code needed to save the current status of the cells to a save file.
 * It also saves the seed of the instance and the x and y size of the grid
 * @param
 * This function takes 4 inputs, a cellStruct of type cell, which contains the settings for each cell
 * The x and y size of the window
 * The seed passed in either by the user, from the save, or from the time on the machine
 * @return
 * This function returns nothing.
 */
int save(cell **cellStruct, int x, int y, int seed);

/**   ----------------------------------------------------------
 * This function contains the code needed to generate a game of life with user directed values like a seed, and the x and
 * y dimensions of the game
 * @param
 * This function takes 1 input, the seed, which is used to create consistent worlds
 * @return
 * This function a 1 upon succesful completion
 */
int userCreated(int seed);

/**   ----------------------------------------------------------
 * This function contains the code needed to randomly generate a game of life with no user input, the randomness/seed
 * comes from the time on the local machine
 * @param
 * This function takes no inputs
 * @return
 * This function returns nothing.
 */
int randomGenerateLife();

/**   ----------------------------------------------------------
 * This function contains the code needed to display the window in curses/cygwin
 * @param
 * This function takes 4 inputs, the height of the window, the width of the window, the startx and starty positions of
 * the game
 * @return
 * This function returns the window created.
 */
WINDOW *create_newwin(int height, int width, int starty, int startx);

int readScenario(int scenarioRead, cell **cellStruct);

#endif //MYEXE_FINALPROJ_H
