//
// Created by David on 10/29/2018.
//

#include "finalProj.h"

/**   ----------------------------------------------------------
 * This function contains the code needed to generate the alive or dead status of a individual cell
 * The rand functions are affected by the seed passed in, so that the user can have either the same experiance
 * or a different experience every time.
 * @param
 * This function takes 4 inputs, a cellStruct of type cell, which contains the settings for each cell
 * The x and y size of the window
 * The seed passed in either by the user, from the save, or from the time on the machine
 * @return
 * This function returns nothing.
 */
void populate(cell **cellStruct, int x, int y, int seed) {
    int randVal = 0;
    srand(seed);
    //also assigns the position of the cell, this is redundant but will stay because I think it breaks if its not there
    //generates a 0 or 1 and assigns this to the cell
    for (int i = 0; i < (x * y); ++i) {
        randVal = rand() % 2;
        cellStruct[i]->position = i;
        cellStruct[i]->status = randVal;
    }

    assignPartners(cellStruct, x, y);
}

/**   ----------------------------------------------------------
 * This function contains the code needed to assign each individual cell its partners. The partners are defined as
 * any cell within the 3x3 radius around the targeted individual cell. Partner cells are passed as the pointer to the
 * status of a partner cell. Cells on the edge or without a full 8 set of partners are assigned a partner who's status
 * is always dead.
 * @param
 * This function takes 4 inputs, a cellStruct of type cell, which contains the settings for each cell
 * The x and y size of the window
 * @return
 * This function returns nothing.
 */
void assignPartners(cell **cellStruct, int x, int y) {
    int *deadVar = malloc(sizeof(int));
    *deadVar = 0;
    int height = y;
    int width = x;
    int yLeftCount = 1;
    int yRightCount = 1;
    int starty = (LINES - height) / 2;
    int startx = (COLS - width) / 2;
    for (int i = 0; i < (x * y); ++i) {
        //check top row
        if (i >= 0 && i <= (x - 1)) {
            if (i == 0) {
                //checks if top left corner
//                int starty = (LINES - height) / 2;
//                int startx = (COLS - width) / 2;
                cellStruct[i]->partnerOne = &cellStruct[i + 1]->status;
                cellStruct[i]->partnerTwo = &cellStruct[i + x]->status;
                cellStruct[i]->partnerThree = &cellStruct[i + x + 1]->status;
                cellStruct[i]->partnerFour = deadVar;
                cellStruct[i]->partnerFive = deadVar;
                cellStruct[i]->partnerSix = deadVar;
                cellStruct[i]->partnerSeven = deadVar;
                cellStruct[i]->partnerEight = deadVar;
                cellStruct[i]->xPos = startx;
                cellStruct[i]->yPos = starty;
            } else if (i == (x - 1)) {
                //checks if  top right hand corner
                cellStruct[i]->partnerOne = &cellStruct[i - 1]->status;
                cellStruct[i]->partnerTwo = &cellStruct[i + x - 1]->status;
                cellStruct[i]->partnerThree = &cellStruct[i + x]->status;
                cellStruct[i]->partnerFour = deadVar;
                cellStruct[i]->partnerFive = deadVar;
                cellStruct[i]->partnerSix = deadVar;
                cellStruct[i]->partnerSeven = deadVar;
                cellStruct[i]->partnerEight = deadVar;
                cellStruct[i]->xPos = startx + i;
                cellStruct[i]->yPos = starty;

            } else {
                //everything else in top row
                cellStruct[i]->partnerOne = &cellStruct[i - 1]->status;
                cellStruct[i]->partnerTwo = &cellStruct[i + 1]->status;
                cellStruct[i]->partnerThree = &cellStruct[i + x - 1]->status;
                cellStruct[i]->partnerFour = &cellStruct[i + x]->status;
                cellStruct[i]->partnerFive = &cellStruct[i + x + 1]->status;
                cellStruct[i]->partnerSix = deadVar;
                cellStruct[i]->partnerSeven = deadVar;
                cellStruct[i]->partnerEight = deadVar;
                cellStruct[i]->xPos = startx + i;
                cellStruct[i]->yPos = starty;
            }
        }
            //check bottom row
        else if (i >= ((x * y) - x) && i <= ((x * y) - 1)) {
            if (i == ((x * y) - x)) {
                //checks if bottom left hand corner
                cellStruct[i]->partnerOne = &cellStruct[i - x]->status;
                cellStruct[i]->partnerTwo = &cellStruct[i - x + 1]->status;
                cellStruct[i]->partnerThree = &cellStruct[i + 1]->status;
                cellStruct[i]->partnerFour = deadVar;
                cellStruct[i]->partnerFive = deadVar;
                cellStruct[i]->partnerSix = deadVar;
                cellStruct[i]->partnerSeven = deadVar;
                cellStruct[i]->partnerEight = deadVar;
                cellStruct[i]->xPos = startx;
                cellStruct[i]->yPos = starty + y - 1;
            } else if (i == ((x * y) - 1)) {
                //checks if bottom right hand corner
                cellStruct[i]->partnerOne = &cellStruct[i - x - 1]->status;
                cellStruct[i]->partnerTwo = &cellStruct[i - x]->status;
                cellStruct[i]->partnerThree = &cellStruct[i - 1]->status;
                cellStruct[i]->partnerFour = deadVar;
                cellStruct[i]->partnerFive = deadVar;
                cellStruct[i]->partnerSix = deadVar;
                cellStruct[i]->partnerSeven = deadVar;
                cellStruct[i]->partnerEight = deadVar;
                cellStruct[i]->xPos = startx + x - 1;
                cellStruct[i]->yPos = starty + y - 1;
            } else {
                //everything else in bottom row
                cellStruct[i]->partnerOne = &cellStruct[i - x - 1]->status;
                cellStruct[i]->partnerTwo = &cellStruct[i - x]->status;
                cellStruct[i]->partnerThree = &cellStruct[i - x + 1]->status;
                cellStruct[i]->partnerFour = &cellStruct[i - 1]->status;
                cellStruct[i]->partnerFive = &cellStruct[i + 1]->status;
                cellStruct[i]->partnerSix = deadVar;
                cellStruct[i]->partnerSeven = deadVar;
                cellStruct[i]->partnerEight = deadVar;
                cellStruct[i]->xPos = startx + x - ((x * y) - i);
                cellStruct[i]->yPos = starty + y - 1;
            }
        }
            //check left
        else if (i % x == 0 && i != 0) {
            cellStruct[i]->partnerOne = &cellStruct[i - x]->status;
            cellStruct[i]->partnerTwo = &cellStruct[i - x + 1]->status;
            cellStruct[i]->partnerThree = &cellStruct[i + 1]->status;
            cellStruct[i]->partnerFour = &cellStruct[i + x]->status;
            cellStruct[i]->partnerFive = &cellStruct[i + x + 1]->status;
            cellStruct[i]->partnerSix = deadVar;
            cellStruct[i]->partnerSeven = deadVar;
            cellStruct[i]->partnerEight = deadVar;
            cellStruct[i]->xPos = startx;
            cellStruct[i]->yPos = starty + yLeftCount;
            yLeftCount++;
        }
            //check right side
        else if ((i - (x - 1)) % x == 0 && i != (x - 1)) {
            cellStruct[i]->partnerOne = &cellStruct[i - x - 1]->status;
            cellStruct[i]->partnerTwo = &cellStruct[i - x]->status;
            cellStruct[i]->partnerThree = &cellStruct[i - 1]->status;
            cellStruct[i]->partnerFour = &cellStruct[i + x - 1]->status;
            cellStruct[i]->partnerFive = &cellStruct[i + x]->status;
            cellStruct[i]->partnerSix = deadVar;
            cellStruct[i]->partnerSeven = deadVar;
            cellStruct[i]->partnerEight = deadVar;
            cellStruct[i]->xPos = startx + x - 1;
            cellStruct[i]->yPos = starty + yRightCount;
            yRightCount++;
        } else {
            cellStruct[i]->partnerOne = &cellStruct[i - (x + 1)]->status;
            cellStruct[i]->partnerTwo = &cellStruct[i - (x)]->status;
            cellStruct[i]->partnerThree = &cellStruct[i - (x - 1)]->status;
            cellStruct[i]->partnerFour = &cellStruct[i - 1]->status;
            cellStruct[i]->partnerFive = &cellStruct[i + 1]->status;
            cellStruct[i]->partnerSix = &cellStruct[i + (x - 1)]->status;
            cellStruct[i]->partnerSeven = &cellStruct[i + x]->status;
            cellStruct[i]->partnerEight = &cellStruct[i + (x + 1)]->status;
            cellStruct[i]->xPos = startx + x - ((x * yLeftCount) - i);
            cellStruct[i]->yPos = starty + yLeftCount - 1;
        }
    }
    fflush(stdout);
//    for (int j = 0; j < (x * y); ++j) {
//        printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n", cellStruct[j]->position, cellStruct[j]->status,
//               *cellStruct[j]->partnerOne, *cellStruct[j]->partnerTwo, *cellStruct[j]->partnerThree,
//               *cellStruct[j]->partnerFour,
//               *cellStruct[j]->partnerFive, *cellStruct[j]->partnerSix, *cellStruct[j]->partnerSeven,
//               *cellStruct[j]->partnerEight);
//    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to determine the status of the surrounding cells and apply the rules to the cell
 * The rules are:
 *  Any live cell with fewer than two live neighbors dies, as if by underpopulation.
    Any live cell with two or three live neighbors lives on to the next generation.
    Any live cell with more than three live neighbors dies, as if by overpopulation.
    Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
 * @param
 * This function takes 2 inputs, a cellStruct of type cell, which contains the settings for each cell
 * The length of the cell which is the x * y of the window
 * @return
 * This function returns nothing.
 */
void deadOrAlive(cell **cellStruct, int length) {
    int sum = 0;
    int result = 0;
    int buffer[length];
    //calculates the number of partner cells alive
    for (int j = 0; j < length; ++j) {
        sum = *cellStruct[j]->partnerOne + *cellStruct[j]->partnerTwo + *cellStruct[j]->partnerThree +
              *cellStruct[j]->partnerFour + *cellStruct[j]->partnerFive + *cellStruct[j]->partnerSix +
              *cellStruct[j]->partnerSeven +
              *cellStruct[j]->partnerEight;
        //Any live cell with fewer than two live neighbors dies, as if by underpopulation
        if (cellStruct[j]->status == 1 && sum < 2) {
            result = 0;
        }
        //Any live cell with two or three live neighbors lives on to the next generation.
        if (cellStruct[j]->status == 1 && (sum == 2 || sum == 3)) {
            result = 1;
        }
        //Any live cell with more than three live neighbors dies, as if by overpopulation.
        if (cellStruct[j]->status == 1 && sum > 3) {
            result = 0;
        }
        if (cellStruct[j]->status == 0 && sum == 3) {
            result = 1;
        }
        buffer[j] = result;
        result = 0;
        sum = 0;
    }
    for (int i = 0; i < length; ++i) {
        cellStruct[i]->status = buffer[i];
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to draw on the screen the position and status of each cell
 * What is drawn is determined by a global unicode in the finalProj.h file
 * @param
 * This function takes 3 inputs, a cellStruct of type cell
 * The x and y size of the window
 * @return
 * This function returns nothing.
 */
void draw(cell **cellStruct, int x, int y) {

    for (int k = 0; k < (x * y); ++k) {

        if (cellStruct[k]->status == 0) {
            mvprintw(cellStruct[k]->yPos, cellStruct[k]->xPos, DEADUNICODE);
        } else {
            mvprintw(cellStruct[k]->yPos, cellStruct[k]->xPos, ALIVEUNICODE);
        }
    }

}

/**   ----------------------------------------------------------
 * This function contains the code needed to load a scenario from a save or scenario file
 * @param
 * This function takes no parameters
 * @return
 * This function a 1 should the function execute correctly
 */
int loadScenario() {
    /**This function takes a user inputted scenario request, navigates to that point in the file, and reads
     * the data from the file into the array assigned
     */
//    int choice = 0;
//    mvprintw(2, 0, "Please load one of the following.");
//    mvprintw(3, 0, "Load from scenario 1: Press 1.");
//    mvprintw(4, 0, "Load from scenario 2: Press 2.");
//    mvprintw(5, 0, "Load from save: Press 3.");
//    refresh();
//    scanf("%d", &choice);
//    clear();
//    if(choice != 3 && choice != 0){
//        readScenario(int scenarioRead, cell** cellStruct);
//
//    }
//    if (choice == 3) {
    FILE *fptr;
    int x;
    int seed = 0;
    int y;

    //opens the file or scenario selected
    fptr = fopen(FILENAME, "r+");
    if (fptr == NULL) //if file does not exist, create it
    {
        fptr = fopen(FILENAME, "w+");
        if (fptr == NULL) {
            mvprintw(2, 0, "Could not open the save file, please save a file first or try again");
            return 1;
        }
    }
    //gets the x and y of the save
    fscanf(fptr, "%d, %d", &x, &y);
    //creates the struct with each cell in it
    cell **cellStruct = malloc(sizeof(cell *) * x * y * 5);
    for (int i = 0; i < (x * y); ++i) {
        cellStruct[i] = malloc(sizeof(cell));
    }

    seed = loadSave(cellStruct);

    WINDOW *my_win;
    int ticks = 0;

    //starts window loading
    int ch;
    int height = y;
    int width = x;
    int starty = (LINES - height) / 2;
    int startx = (COLS - width) / 2;

    //displays messages constant to window
    printw("Hold F1 to exit");
    mvprintw(2, 0, "The resolution of the board is: %d x %d", x, y);
    mvprintw(3, 0, "The seed is: %d", seed);
    mvprintw(4, 0, "Press 'r' to restart with a new sim.");
    mvprintw(6, 0, "Press 's' to save");


    refresh();
    int mousey = 0;
    int mousex = 0;

    my_win = create_newwin(height, width, starty, startx);

    //main loop that doesn't exit without the press of F1
    while ((ch = getch()) != KEY_F(1)) {
        mvprintw(1, 0, "Cycles: %d", ticks);
        wrefresh(my_win);
        delwin(my_win);
        deadOrAlive(cellStruct, (x * y));
        draw(cellStruct, x, y);
        my_win = create_newwin(height, width, starty, startx);
        ticks++;
        getyx(my_win, mousey, mousex);
        mvprintw(5, 0, "Current Mouse Coords are: x.%d & y.%d", mousex, mousey);

        //looks for a r or s to create a new game of life or save to a file

        if ((ch = getch()) == 'r') {
            clear();
            free(cellStruct);
            if (loadScenario() == 0) {
                break;
            }
        }
        if ((ch = getch()) == 's') {
            save(cellStruct, x, y, seed);
        }
    }
    endwin();
    return 0;
    //    save(cellStruct, x, y);
}
//}

/**   ----------------------------------------------------------
 * This function contains the code needed to save the current status of the cells to a save file.
 * It also saves the seed of the instance and the x and y size of the grid
 * @param
 * This function takes 4 inputs, a cellStruct of type cell, which contains the settings for each cell
 * The x and y size of the window
 * The seed passed in either by the user, from the save, or from the time on the machine
 * @return
 * This function returns nothing.
 */
int save(cell **cellStruct, int x, int y, int seed) {
    FILE *fptr;
    fptr = fopen(FILENAME, "r+");
    if (fptr == NULL) //if file does not exist, create it
    {
        fptr = fopen(FILENAME, "w+");
        if (fptr == NULL) {
            mvprintw(2, 0, "Could not open the save file, please save a file first or try again");
            return 0;

        }
    }
    //saves the x and y from the current game
    fprintf(fptr, "%d, %d\n", x, y);
    //saves the seed of the current game
    fprintf(fptr, "%d\n", seed);
    //saves the position for the cell and its status to the file
    for (int i = 0; i < (x * y); ++i) {
        fprintf(fptr, "%d, %d\n", cellStruct[i]->position, cellStruct[i]->status);
    }

    //attempts to close the file
    if (fclose(fptr) != 0) {
        printf("Failure to chooch\n");
        return 0;
    }
    return 1;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to load from a save all the data necessary to recreate the previous save
 * @param
 * This function takes 4 inputs, a cellStruct of type cell, which contains the settings for each cell
 * @return
 * This function the seed from the loaded save
 */
int loadSave(cell **cellStruct) {
    FILE *fptr;
    int seed = 0;
    int x;
    int y;
    fptr = fopen(FILENAME, "r+");
    if (fptr == NULL) //if file does not exist, create it
    {
        fptr = fopen(FILENAME, "w+");
        if (fptr == NULL) {
            printf("Could not open file\n");
        }
    }
    //grabs x and y coords from file
    fscanf(fptr, "%d, %d", &x, &y);
    //grabs the seed from the file
    fscanf(fptr, "%d", &seed);
    //populates the new struct with the data from the file
    for (int i = 0; i < (x * y); ++i) {
        fscanf(fptr, "%d, %d", &cellStruct[i]->position, &cellStruct[i]->status);
    }
    //assigns partners with the read data
    assignPartners(cellStruct, x, y);

    //closes file
    if (fclose(fptr) != 0) {
        printf("Failure to chooch\n");
    }
    return seed;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to generate a game of life with user directed values like a seed, and the x and
 * y dimensions of the game
 * @param
 * This function takes 1 input, the seed, which is used to create consistent worlds
 * @return
 * This function a 1 upon succesful completion
 */
int userCreated(int seed) {
    WINDOW *my_win;
    int x;
    int ticks = 0;
    int y;
    border(0, 0, 0, 0, 0, 0, 0, 0);
    mvprintw(LINES / 4, COLS / 2 - 39, "Please enter a x coord.");
    refresh();
    scanf("%d", &x);
    mvprintw((LINES / 4) + 7, COLS / 2 - 39, "%d", x);
    clear();
    border(0, 0, 0, 0, 0, 0, 0, 0);
    mvprintw((LINES / 4) + 5, COLS / 2 - 39, "Please enter a y coord.");
    refresh();
    scanf("%d", &y);
    mvprintw((LINES / 4) + 7, COLS / 2 - 39, "%d", y);

    clear();

    int ch;
    //creates the struct with each cell in it
    cell **cellStruct = malloc(sizeof(cell *) * x * y * 5);
    for (int i = 0; i < (x * y); ++i) {
        cellStruct[i] = malloc(sizeof(cell));
    }

    //populates the cell with neighbors and status
    populate(cellStruct, x, y, seed);

    //begins window start
    int height = y;
    int width = x;
    int starty = (LINES - height) / 2;
    int startx = (COLS - width) / 2;

    //displays data constant to the window
    printw("Hold F1 to exit");
    mvprintw(2, 0, "The resolution of the board is: %d x %d", x, y);
    mvprintw(3, 0, "The seed is: %d", seed);
    mvprintw(4, 0, "Press 'r' to restart with a new sim.");
    mvprintw(6, 0, "Press 's' to save");

    refresh();


    my_win = create_newwin(height, width, starty, startx);
    while ((ch = getch()) != KEY_F(1)) {
        mvprintw(1, 0, "Cycles: %d", ticks);
        wrefresh(my_win);
        delwin(my_win);
        deadOrAlive(cellStruct, (x * y));
        draw(cellStruct, x, y);
        my_win = create_newwin(height, width, starty, startx);
        ticks++;

        //looks for a r or s to create a new game of life or save to a file
        if ((ch = getch()) == 'r') {
            clear();
            free(cellStruct);
            if (randomGenerateLife() == 0) {
                break;
            }
        }
        if ((ch = getch()) == 's') {
            save(cellStruct, x, y, seed);
        }
    }

    endwin();
    save(cellStruct, x, y, seed);
    //frees memory from cell struct
    free(cellStruct);
    return 1;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to randomly generate a game of life with no user input, the randomness/seed
 * comes from the time on the local machine
 * @param
 * This function takes no inputs
 * @return
 * This function returns nothing.
 */
int randomGenerateLife() {
    WINDOW *my_win;
    time_t seed = time(0);
    srand(seed);
    int x = (rand() % (MIN_VALUE - MAX_VALUE));
    int ticks = 0;
    int y = (rand() % (MIN_VALUE - MAX_VALUEY));

    //creates the struct with each cell in it
    int ch;
    cell **cellStruct = malloc(sizeof(cell *) * x * y * 5);
    for (int i = 0; i < (x * y); ++i) {
        cellStruct[i] = malloc(sizeof(cell));
    }

    //populates the cell with neighbors and status
    populate(cellStruct, x, y, seed);
    //begins window start
    int height = y;
    int width = x;
    int starty = (LINES - height) / 2;
    int startx = (COLS - width) / 2;

    //displays data constant to the window
    printw("Hold F1 to exit");
    mvprintw(2, 0, "The resolution of the board is: %d x %d", x, y);
    mvprintw(3, 0, "The seed is: %d", seed);
    mvprintw(4, 0, "Press 'r' to restart with a new sim.");
    mvprintw(6, 0, "Press 's' to save");

    refresh();

    //Was suppposed to display coordinates of the mouse onto the screen
    int mousey = 0;
    int mousex = 0;

    my_win = create_newwin(height, width, starty, startx);

    //main loop that doesn't exit without the press of F1
    while ((ch = getch()) != KEY_F(1)) {
        mvprintw(1, 0, "Cycles: %d", ticks);
        delwin(my_win);
        deadOrAlive(cellStruct, (x * y));
        draw(cellStruct, x, y);
        wrefresh(my_win);

        my_win = create_newwin(height, width, starty, startx);
        ticks++;
        getyx(my_win, mousey, mousex);
        mvprintw(5, 0, "Current Mouse Coords are: x.%d & y.%d", mousex * 10, mousey * 10);

        //looks for a r or s to create a new game of life or save to a file
        if ((ch = getch()) == 'r') {
            clear();
            free(cellStruct);

            if (randomGenerateLife() == 0) {
                break;
            }
        }
        if ((ch = getch()) == 's') {
            save(cellStruct, x, y, seed);
        }
    }

    endwin();
    save(cellStruct, x, y, seed);
    free(cellStruct);
    return 0;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to display the window in curses/cygwin
 * @param
 * This function takes 4 inputs, the height of the window, the width of the window, the startx and starty positions of
 * the game
 * @return
 * This function returns the window created.
 */
WINDOW *create_newwin(int height, int width, int starty, int startx) {
    WINDOW *local_win;

    local_win = newwin(height, width, starty, startx);
    wrefresh(local_win);

    return local_win;
}

