/** FinalProj.c
* ==============================================================
* Name: David Thacker, 291018
* Section: T3-4
* Project: Final Project - Thacker's Game of Life
* Description: Implementation of Conways Game of Life
* Documentation Statement: None
* ==============================================================
* UserManual/Instructions:
*   Replace this paragraph with instructions on how to build, run,
*   and use your program.  If your program has "built-in" instructions
*   or a tutorial you should say that here and forgo a long description.
 *
 *  Required Programming Skills (must have 4 of 5):
 *   1) Dynamic Memory Allocation
 *          What you did to meet this req: use dynamic memory allocation to create a
 *              File: finalProjFunctions.c
 *              Line#: 44,297,465,538
 *   2) Pointers
 *          What you did to meet this req: Partners of the cells status's are assigned via a pointer
 *              File: finalProjFunctions.c
 *              Line#: 59-173
 *   3) File I/O
 *          What you did to meet this req: User can save their current scenario or load a past saved scenario
 *              File: finalProjFunctions.c
 *              Line#: 285, 371, 410
 *   4) Structs
 *          What you did to meet this req: The cells information is stored in a struct called cell
 *              File: finalProj.h
 *              Line#: 38-51
 *
 *   5) String Manipulation
 *          What you did to meet this req: No string manipulation
 *              File: N/A
 *              Line#: N/A
 *
 *  Required Advanced Programming Skills (1 or more):
 *   1) Recursion
 *          What you did to meet this req: User can request a new randomly generated game of life by pressng r
 *              File: finalProjFunctions.c
 *              Line#: 344, 503, 582
 *
 *   2) 2D Dynamic Memory
 *          What you did to meet this req: The cellStruct is created as a array of pointers to indivudual cellStructs
 *              File: finalProjFunctions.c
 *              Line#: 44,297,465,538
 *
 *   3) Graphics - Curses or Win32
 *          What you did to meet this req: The cells are displayed on screen as a black square or a white square
 *              File: finalProjFunctions.c
 *              Line#: 242-253
 *
 *   List any changes or omissions from stated shall requirements (from
 *   your design) also describe any functionality that isn't working.
 *      Requirement changes/omissions:
 *          1) Load scenario only loads from a previous save.
 *          2) Natural disasters were not implemented
 *          3) Mouse tracking and clicking to change status of cells would not work no matter how much I tried
 *
 *      Errors:
 *          1) Sometimes it crashes and I have no idea why, this occurs after using to much recursion for about a minute
 *              and displays a Seg Fault on screen
 *          2)
 *
 *   How many hours did you spend on this project: 35hrs
 *   How many lines of code did you write in total: 909
 *
*/
#include "finalProj.h"

int main() {
    //sets a default seed
    int seed = 1543970702;
    int choice = 0;
    bool validSelection = false;

    setlocale(LC_ALL, ""); // for use of unicode characters
    initscr(); // Initialize the window
    start_color(); // enable color support
    use_default_colors(); // set default colors
//    noecho(); // Don't echo any keypresses
    curs_set(TRUE); // Don't display a cursor
    echo();
    timeout(DELAY); // Amount of time to wait for keyboard input
    keypad(stdscr, TRUE); // allows use of arrow keys
    for (int i = 0; i < 256; i++) {
        init_pair(i, i, -1); // pair #, color, terminal background
    }

    //Goes while user has not selected a valid input of 1 2 or 3
    while (validSelection == false) {
        border(0, 0, 0, 0, 0, 0, 0, 0);

        mvprintw(LINES / 4, COLS / 2 - 39,
                 "Would you like to enter a seed, load from scenario, or let life take hold?");
        mvprintw((LINES / 4) + 5, COLS / 2 - 39,
                 "Press 1 to enter a seed, 2 to load from save, or 3 for random generation.");
        refresh();
        scanf("%d", &choice);
        clear();

        if (choice == 1) {
            validSelection = true;
            border(0, 0, 0, 0, 0, 0, 0, 0);
            mvprintw((LINES / 4) + 5, COLS / 2 - 39, "Please enter a seed.");
            refresh();
            scanf("%d", &seed);
            mvprintw((LINES / 4) + 7, COLS / 2 - 39, "%d", seed);
            clear();
            userCreated(seed);
        } else if (choice == 2) {
            validSelection = true;
            clear();
            loadScenario();
        } else if (choice == 3) {
            validSelection = true;
            clear();
            randomGenerateLife();
        } else {
            mvprintw(100, 100, "That is not a valid selection.\n");
            refresh();
        }
    }
}