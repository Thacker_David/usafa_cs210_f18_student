//
// Created by C21David.Thacker on 8/30/2018.
//

/** PEX0.c
* ==============================================================
* Name: David Thacker, 082218
* Section: T3-4
* Project: PEX 0 - ASCII Art
* Purpose: Understand how to use C standard library functions,
*          and user-defined functions.
* Documentation Statement: No help recieved.
* ==============================================================
* Instructions:
 * Complete and submit PEX0 as outlined in the writeup.
*/

#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

void drawTree(); //declares function for drawing a tree//
void drawCat();  //declares function for drawing a cat//
double
coursePercentage(double homework, double midterm, double final);  //declares function for finding the course grade//

int main() {

    //This does the logic for selecting a 1 or 2 and drawing tree or cat//
    int startingOneZero;
    scanf("%d", &startingOneZero);
    if (startingOneZero == 1) {
        drawTree();
    } else if (startingOneZero == 2) {
        drawCat();
    }

    //Start of part 3
    int homeworkGrade;
    int midtermExamGrade;
    int finalExamGrade;
    printf("Please enter a grade for homework out of 80\n");
    scanf("%d", &homeworkGrade);
    printf("Please enter a grade for the midterm out of 40\n");
    scanf("%d", &midtermExamGrade);
    printf("Please enter a grade for the final out of 70\n");
    scanf("%d", &finalExamGrade);
    double finalGrade = coursePercentage(homeworkGrade, midtermExamGrade, finalExamGrade);
    printf("%lf", finalGrade);

    return 1;
}

//draws a tree if main input has a 1
void drawTree() {
    printf("   *\n  ***\n *****\n*******\n  ***\n");
}

//draws a cat if main input has a 2
void drawCat() {
    printf("/\\   /\\\n  o o\n =   =\n  ---\n");
}

//calculates course percentages for three inputs given from input//
double coursePercentage(double homework, double midterm, double final) {
    double tempAssn = ((((homework / 80.0) * .20) + ((midterm / 40.0) * .30)) + ((final / 70) * .50)) * 100;
    return tempAssn;
}