/** pex3.h
* ==============================================================
* Name: David Thacker, 300319
* Section: T7
* Project: Spell Check
* Documentation Statement: Helped Manny Riolo on 2d arrays and how the tree's look and function
* ==============================================================
* Instructions:
 * Design, implement, and test, a spell check using a ternary tree
 * Adhere to all programming standards and the PEX3 rubric
*/
//
#ifndef MYEXE_PEX3_H
#define MYEXE_PEX3_H

#include <stdio.h>
#include <stdlib.h>
#include "ternaryTree.h"

/**   ----------------------------------------------------------
 * This function contains the code needed to insert words from a file into an array of strings, this is then used in other
 * functions to compare the strings read in here to those in a ternary tree. This is different from the ternaryTree library function
 * insertWordArray as this function does not need to return.
 * @param
 * This function takes:
 * char filename[]: which is the address of the file with the words to be read in
 * char **wordArray: the pointer to the array of strings to contain the words read from the file
 * int numLines: the number of lines in the file above
 * @return
 * This function returns nothing
 */
void getWords(char filename[], char **wordArray, int numLines);


#endif //MYEXE_PEX3_H
