/** tree.h
* ==============================================================
* Name: David Thacker, 300319
* Section: T7
* Project: Spell Check
* Documentation Statement: Helped Manny Riolo on 2d arrays and how the tree's look and function
* ==============================================================
* Instructions:
 * Design, implement, and test, a spell check using a ternary tree
 * Adhere to all programming standards and the PEX3 rubric
*/

#ifndef MYEXE_TERNARYTREE_H
#define MYEXE_TERNARYTREE_H
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct node {
    char data;
    struct node *left;
    struct node *middle;
    struct node *right;
} Node;

/**   ----------------------------------------------------------
 * This function contains the code needed to get the number of lines in a file
 * @param
 * This function takes:
 * char filename[]: which is the address of the file to have its lines read
 * @return
 * This function returns a 0 if it cannot open the file or count+1 which is the number of lines in the file
 */
int getNumLines(char filename[]);

/**   ----------------------------------------------------------
 * This function contains the code needed to strip the newline and null character from loaded files
 * @param
 * This function takes:
 * char *word: the word which needs to be striped
 * @return
 * This function returns nothing
 */
void stripNewLine(char *word);

/**   ----------------------------------------------------------
 * This function contains the code needed to insert words from a file into an array of strings, this is then used in other
 * functions to compare the strings read in here to those in a ternary tree
 * @param
 * This function takes:
 * char filename[]: which is the address of the file with the words to be read in
 * char **wordArray: the pointer to the array of strings to contain the words read from the file
 * int numLines: the number of lines in the file above
 * @return
 * This function returns a -1 if the number of lines is less than or equal than 1 a 0 if the file could not be opened
 * and a 1 for successful completion
 */
int insertWordArray(char filename[], char **wordArray, int numLines);

/**   ----------------------------------------------------------
 * This function contains the code needed to insert the characters of a word into a tree
 * @param
 * This function takes:
 * Node **node: which is a pointer to the root of a ternary tree of nodes
 * char *word: the word which is having its characters inserted in
 * @return
 * This function returns nothing
 */
void insertTree(Node **node, char *word);

/**   ----------------------------------------------------------
 * This function contains the code to create a new node
 * @param
 * This function takes:
 * char data: which is the data to be inserted into the new node
 * @return
 * This function returns the pointer to the new node
 */
Node *createTree(char data);

/**   ----------------------------------------------------------
 * This function contains the code needed to search for a word in a ternary tree
 * @param
 * This function takes:
 * Node **node: which is a pointer to the root of a ternary tree of nodes
 * char *word: the word which for which is to be searched in the tree
 * @return
 * This function returns a true if the word is in the tree or a false if it is not
 */
bool searchTree(Node **node, char *word);

/**   ----------------------------------------------------------
 * This function contains the code needed to delete all nodes in a ternary tree
 * @param
 * This function takes:
 * Node **node: which is a pointer to the root of a ternary tree of nodes
 * @return
 * This function returns nothing
 */
void deleteTree(Node **node);

/**   ----------------------------------------------------------
 * This function contains the code needed to optimally insert into a tree words from a dictionary starting from the middle
 * word and then recursively inserting each smaller set's middle part until reaching the end and inserting back out
 * @param
 * This function takes a pointer to a pointer of type node, which is the root of a ternary tree Node **node
 * The char **word which is the pointer to an array of strings containing the words previously read in
 * The ints min and max which contain the min and max index's of the array to check or insert between
 * @return
 * This function returns nothing
 */
void optimizeTree(Node **node, char **word, int min, int max);

/**   ----------------------------------------------------------
 * This function contains the code needed to calculate the number of nodes in a ternary tree
 * @param
 * This function takes a pointer to a pointer of type node, which is the root of a ternary tree
 * @return
 * This function returns the number of nodes in the entire tree starting with the root
 */
int getNumNodes(Node **node);

#endif //MYEXE_TERNARYTREE_H
