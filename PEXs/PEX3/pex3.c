/** pex3.c
* ==============================================================
* Name: David Thacker, 300319
* Section: T7
* Project: Spell Check
* Documentation Statement: Helped Manny Riolo on 2d arrays and how the tree's look and function
* ==============================================================
* Instructions:
 * Design, implement, and test, a spell check using a ternary tree
 * Adhere to all programming standards and the PEX3 rubric
*/
//
#include "pex3.h"
#include "ternaryTree.h"

int main(int argc, char *argv[]) {
    int numLines = getNumLines(argv[1]);
    printf("%d\n", numLines);
    char **wordArray = malloc(sizeof(char *) * numLines);
    printf("%d\n", insertWordArray(argv[1], wordArray, numLines));
    for (int i = 0; i < numLines; ++i) {
        stripNewLine(wordArray[i]);
    }

    Node *tree = NULL;
    optimizeTree(&tree, wordArray, 0, numLines);
    int testNumLines = getNumLines(argv[2]) - 1;
    char **testArray = malloc(sizeof(char *) * testNumLines);
    getWords(argv[2], testArray, testNumLines);
    for (int j = 0; j < testNumLines; ++j) {
        printf("%s is spelled correctly or in the dictionary: %d\n", testArray[j], searchTree(&tree, testArray[j]));
    }
    return 1;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to insert words from a file into an array of strings, this is then used in other
 * functions to compare the strings read in here to those in a ternary tree. This is different from the ternaryTree library function
 * insertWordArray as this function does not need to return.
 * @param
 * This function takes:
 * char filename[]: which is the address of the file with the words to be read in
 * char **wordArray: the pointer to the array of strings to contain the words read from the file
 * int numLines: the number of lines in the file above
 * @return
 * This function returns nothing
 */
void getWords(char filename[], char **wordArray, int numLines) {
    FILE *fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
    }

    for (int i = 0; i < numLines; ++i) {
        wordArray[i] = malloc(sizeof(char *) * 50);
        fscanf(fp, "%s", wordArray[i]);
    }
    // Close the file
    if (fclose(fp) != 0) {
        printf("Failure to chooch\n");
    }

}
