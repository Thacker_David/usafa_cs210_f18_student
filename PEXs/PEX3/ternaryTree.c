/** tree.c
* ==============================================================
* Name: David Thacker, 300319
* Section: T7
* Project: Spell Check
* Documentation Statement: Helped Manny Riolo on 2d arrays and how the tree's look and function
* ==============================================================
* Instructions:
 * Design, implement, and test, a spell check using a ternary tree
 * Adhere to all programming standards and the PEX3 rubric
*/

#include "ternaryTree.h"

/**   ----------------------------------------------------------
 * This function contains the code needed to calculate the number of nodes in a ternary tree
 * @param
 * This function takes a pointer to a pointer of type node, which is the root of a ternary tree
 * @return
 * This function returns the number of nodes in the entire tree starting with the root
 */
int getNumNodes(Node **node) {
    int count = 1;
    if (((*node)->left) != NULL) {
        count += getNumNodes(&((*node)->left));
    }
    if (((*node)->middle) != NULL) {
        count += getNumNodes(&((*node)->middle));
    }
    if (((*node)->right) != NULL) {
        count += getNumNodes(&((*node)->right));
    }
    return count;

}

/**   ----------------------------------------------------------
 * This function contains the code needed to optimally insert into a tree words from a dictionary starting from the middle
 * word and then recursively inserting each smaller set's middle part until reaching the end and inserting back out
 * @param
 * This function takes a pointer to a pointer of type node, which is the root of a ternary tree Node **node
 * The char **word which is the pointer to an array of strings containing the words previously read in
 * The ints min and max which contain the min and max index's of the array to check or insert between
 * @return
 * This function returns nothing
 */
void optimizeTree(Node **node, char **word, int min, int max) {
    if (max - min == 1) {
        printf("Inserting \"%s\"\n", word[min]);
        insertTree(node, word[min]);
        return;
    } else if (max - min == 0) {
        printf("Inserting \"%s\"\n", word[max]);
        insertTree(node, word[max]);
        return;
    } else {
        int split = (max + min) / 2;
        printf("Inserting \"%s\"\n", word[split]);
        insertTree(node, word[split]);
        optimizeTree(node, word, min, split);
        if (max - min != 2) {
            optimizeTree(node, word, split + 1, max);
        }
    }

}

/**   ----------------------------------------------------------
 * This function contains the code needed to insert words from a file into an array of strings, this is then used in other
 * functions to compare the strings read in here to those in a ternary tree
 * @param
 * This function takes:
 * char filename[]: which is the address of the file with the words to be read in
 * char **wordArray: the pointer to the array of strings to contain the words read from the file
 * int numLines: the number of lines in the file above
 * @return
 * This function returns a -1 if the number of lines is less than or equal than 1 a 0 if the file could not be opened
 * and a 1 for successful completion
 */
int insertWordArray(char filename[], char **wordArray, int numLines) {
    if (numLines <= 1) {
        return -1;
    }
    FILE *fp = fopen(filename, "r");
    int c;
    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
        return 0;
    }
    for (int i = 0; i < numLines; ++i) {
        wordArray[i] = malloc(sizeof(wordArray) * 50);
        int j = 0;

        while ((c = fgetc(fp)) != EOF) {
            wordArray[i][j] = (char) c;
            j++;
            if (c == '\n') {
                break;
            }
        }

    }
    return 1;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to get the number of lines in a file
 * @param
 * This function takes:
 * char filename[]: which is the address of the file to have its lines read
 * @return
 * This function returns a 0 if it cannot open the file or count+1 which is the number of lines in the file
 */
int getNumLines(char filename[]) {
    char c;
    int count = 0;
    // Open the file
    FILE *fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
        return 0;
    }

    // Extract characters from file and store in character c
    for (c = getc(fp); c != EOF; c = getc(fp))
        if (c == '\n') // Increment count if this character is newline
            count = count + 1;

    // Close the file
    fclose(fp);

    return count + 1;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to strip the newline and null character from loaded files
 * @param
 * This function takes:
 * char *word: the word which needs to be striped
 * @return
 * This function returns nothing
 */
void stripNewLine(char *word) {
    char *newLine;
    if ((newLine = strchr(word, '\n')) != NULL) {
        *newLine = '\0';
    }
    if ((newLine = strchr(word, '\r')) != NULL) {
        *newLine = '\0';
    }
}

/**   ----------------------------------------------------------
 * This function contains the code to create a new node
 * @param
 * This function takes:
 * char data: which is the data to be inserted into the new node
 * @return
 * This function returns the pointer to the new node
 */
Node *createTree(char data) {
    Node *newNode;
    newNode = malloc(sizeof(Node));
    newNode->data = data;
    newNode->left = NULL;
    newNode->middle = NULL;
    newNode->right = NULL;
    return newNode;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to insert the characters of a word into a tree
 * @param
 * This function takes:
 * Node **node: which is a pointer to the root of a ternary tree of nodes
 * char *word: the word which is having its characters inserted in
 * @return
 * This function returns nothing
 */
void insertTree(Node **node, char *word) {
    if (*node == NULL) {
        if (word[0] == '\0') {
            *node = createTree(word[0]);
            return;
        }
        *node = createTree(word[0]);
        insertTree(&((*node)->middle), &word[1]);
        return;
    } else if (word[0] > (*node)->data) {
        insertTree(&((*node)->right), &word[0]);
        return;
    } else if (word[0] < (*node)->data) {
        insertTree(&((*node)->left), &word[0]);
        return;
    } else if (word[0] == (*node)->data) {
        insertTree(&((*node)->middle), &word[1]);
        return;
    }

}

/**   ----------------------------------------------------------
 * This function contains the code needed to delete all nodes in a ternary tree
 * @param
 * This function takes:
 * Node **node: which is a pointer to the root of a ternary tree of nodes
 * @return
 * This function returns nothing
 */
void deleteTree(Node **node) {
    if (*node == NULL) {
        return;
    }
    deleteTree(&((*node)->left));
    deleteTree(&((*node)->middle));
    deleteTree(&((*node)->right));
    printf("Deleting Node %c\n", (*node)->data);
    free(*node);
    *node = NULL;

}

/**   ----------------------------------------------------------
 * This function contains the code needed to search for a word in a ternary tree
 * @param
 * This function takes:
 * Node **node: which is a pointer to the root of a ternary tree of nodes
 * char *word: the word which for which is to be searched in the tree
 * @return
 * This function returns a true if the word is in the tree or a false if it is not
 */
bool searchTree(Node **node, char *word) {
    if (*node == NULL) {
        return false;
    }
    if (word[0] == '\0' && (*node)->data == '\0') {
        return true;
    } else if (word[0] > (*node)->data) {
        searchTree(&((*node)->right), &word[0]);
    } else if (word[0] < (*node)->data) {
        searchTree(&((*node)->left), &word[0]);
    } else if (word[0] == (*node)->data) {
        searchTree(&((*node)->middle), &word[1]);
    }
}