/** ternaryTreeUnitTest.c
* ==============================================================
* Name: David Thacker, 300319
* Section: T7
* Project: Spell Check
* Documentation Statement: Helped Manny Riolo on 2d arrays and how the tree's look and function
* ==============================================================
* Instructions:
 * Design, implement, and test, a spell check using a ternary tree
 * Adhere to all programming standards and the PEX3 rubric
*/

//TODO Unit Tests
#include "ternaryTree.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "pex3.h"

/**   ----------------------------------------------------------
 * This function contains the code needed to check for balance before insertion into a tree accordance with the unit test document provided in the folder
 * @param
 * This function takes:
 * Nothing
 * @return
 * This function returns a 1 if everything passed, a 0 if insertWordArray fails to properly load the files
 */
int balancedInsertion();

/**   ----------------------------------------------------------
 * This function contains the code needed to check for proper insertion into a tree in accordance with the unit test document provided in the folder
 * @param
 * This function takes:
 * Nothing
 * @return
 * This function returns a 1 if everything passed, a -1 if the first test failed, a -2 if the second test failed, and a -3 if the third test failed
 */
int treeInsertion();

/**   ----------------------------------------------------------
 * This function contains the code needed to check for proper spell check in accordance with the unit test document provided in the folder
 * @param
 * This function takes:
 * Nothing
 * @return
 * This function returns nothing cause I figured it prints error messages why have checks back in main that's dumb
 */
void spellCheck();

int getWords(char filename[], char **wordArray, int numLines);

int main() {
    spellCheck();
    int balancedCheck = balancedInsertion();
    if (balancedCheck != 1) {
        if (balancedCheck == 0) {
            printf("There was a failure in the insertWordArray\n ");
        }
    }
    int treeInsertionCheck = treeInsertion();
    if (treeInsertionCheck != 1) {
        if (treeInsertionCheck == -1) {
            printf("The first test failed\n");
        } else {
            printf("The first test passed\n");
        }
        if (treeInsertionCheck == -2) {
            printf("The second test failed\n");
        } else {
            printf("The second test passed\n");
        }
        if (treeInsertionCheck == -3) {
            printf("The third test failed\n");
        } else {
            printf("The third test passed\n");
        }
    } else {
        printf("All insertion tests finished successfully\n ");

    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to check for balance before insertion into a tree accordance with the unit test document provided in the folder
 * @param
 * This function takes:
 * Nothing
 * @return
 * This function returns a 1 if everything passed, a 0 if insertWordArray fails to properly load the files
 */
int balancedInsertion() {
    //test for balanced tree resulting in COP, CAR, CAD, CAB, CAT, COW, COT, CUT
    printf("\nStart balance check test 1\n");
    char *FILENAME = "../PEXs/PEX3/test_dictionary.txt";
    printf("\nChecking test dictionary: %s\n", FILENAME);

    int numLines = getNumLines(FILENAME);
    printf("\nThe number of lines in this file is %d\n", numLines);
    char **wordArray = malloc(sizeof(char *) * numLines);
    if (insertWordArray(FILENAME, wordArray, numLines) != 1) {
        return 0;
    }

    for (int i = 0; i < numLines; ++i) {
        stripNewLine(wordArray[i]);
    }

    Node *tree = NULL;
    printf("Expecting:\n");
    printf("COP\n");
    printf("CAR\n");
    printf("CAD\n");
    printf("CAB\n");
    printf("CAT\n");
    printf("COW\n");
    printf("COT\n");
    printf("CUT\n");


    optimizeTree(&tree, wordArray, 0, numLines);

    //test for balanced insertion of 0 things in the tree, returns a 0 if insertWordArray would generate errors
    printf("\nStart balance check test 2\n");
    FILENAME = "../PEXs/PEX3/test_dictionary1.txt";
    printf("\nChecking test dictionary: %s\n", FILENAME);

    numLines = getNumLines(FILENAME);
    printf("\nThe number of lines in this file is %d\n", numLines);
    wordArray = malloc(sizeof(char *) * numLines);
    printf("Expecting:\nFailure Message\n");

    if (insertWordArray(FILENAME, wordArray, numLines) != 1) {
        printf("The requested list is out of bounds or the file does not exist\n");
        return 0;
    }

    for (int i = 0; i < numLines; ++i) {
        stripNewLine(wordArray[i]);
    }

    tree = NULL;
    optimizeTree(&tree, wordArray, 0, numLines);

    return 1;

}

/**   ----------------------------------------------------------
 * This function contains the code needed to check for proper insertion into a tree in accordance with the unit test document provided in the folder
 * @param
 * This function takes:
 * Nothing
 * @return
 * This function returns a 1 if everything passed, a -1 if the first test failed, a -2 if the second test failed, and a -3 if the third test failed
 */
int treeInsertion() {
    //THIS SECTION DOES TEST ON THE STANDARD TEST DICTIONARY TO SEE IF THE INSERTION RESULTS IN THE CORRECT NUMBER OF NODES
    //CORRECT OUTPUT FOR NODES IS 20
    printf("\nStart insertion check test 1\n");

    char *FILENAME = "../PEXs/PEX3/test_dictionary.txt";
    printf("\nChecking test dictionary: %s\n", FILENAME);

    int numLines = getNumLines(FILENAME);
    printf("\nThe number of lines in this file is %d\n", numLines);
    char **wordArray = malloc(sizeof(char *) * numLines);
    if (insertWordArray(FILENAME, wordArray, numLines) != 1) {
        return 0;
    }

    for (int i = 0; i < numLines; ++i) {
        stripNewLine(wordArray[i]);
    }

    Node *tree = NULL;
    optimizeTree(&tree, wordArray, 0, numLines);
    int numNodes = getNumNodes(&tree);

    printf("The number of nodes in the tree is %d\n", numNodes);
    if (numNodes != 20) {
        return 0;
    }

    //THIS SECTION DOES A SECOND TEST ON A SMALLER DICTIONARY TO SEE IF SMALLER VALUES STILL HOLD TRUE
    //CORRECT OUTPUT FOR NODES IS 10
    printf("\nStart insertion check test 2\n");
    FILENAME = "../PEXs/PEX3/test2.txt";
    printf("\nChecking test dictionary: %s\n", FILENAME);

    numLines = getNumLines(FILENAME);
    printf("\nThe number of lines in this file is %d\n", numLines);
    wordArray = malloc(sizeof(char *) * numLines);
    if (insertWordArray(FILENAME, wordArray, numLines) != 1) {
        return -1;
    }

    for (int i = 0; i < numLines; ++i) {
        stripNewLine(wordArray[i]);
    }

    tree = NULL;
    optimizeTree(&tree, wordArray, 0, numLines);
    numNodes = getNumNodes(&tree);
//    assert(numNodes == 20);

    printf("The number of nodes in the tree is %d\n", numNodes);
    if (numNodes != 11) {
        return -2;
    }

    printf("\nStart insertion check test 3\n");

    //THIS SECTION DOES A THIRD
    FILENAME = "../PEXs/PEX3/test_dictionary1.txt";
    printf("\nChecking test dictionary: %s\n", FILENAME);
    numLines = getNumLines(FILENAME);
    printf("\nThe number of lines in this file is %d\n", numLines);
    wordArray = malloc(sizeof(char *) * numLines);
    if (insertWordArray(FILENAME, wordArray, numLines) != 1) {
        return 0;
    }

    for (int i = 0; i < numLines; ++i) {
        stripNewLine(wordArray[i]);
    }

    tree = NULL;
    optimizeTree(&tree, wordArray, 0, numLines);
    numNodes = getNumNodes(&tree);
//    assert(numNodes == 20);

    printf("The number of nodes in the tree is %d\n", numNodes);
    if (numNodes != 0) {
        return -3;
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to check for proper spell check in accordance with the unit test document provided in the folder
 * @param
 * This function takes:
 * Nothing
 * @return
 * This function returns nothing cause I figured it prints error messages why have checks back in main that's dumb
 */
void spellCheck() {
    char *FILENAME = "../PEXs/PEX3/dictionary.txt";
    printf("\nChecking test dictionary: %s\n", FILENAME);

    int numLines = getNumLines(FILENAME);
    printf("%d\n", numLines);
    char **wordArray = malloc(sizeof(char *) * numLines);
    printf("%d\n", insertWordArray(FILENAME, wordArray, numLines));
    for (int i = 0; i < numLines; ++i) {
        stripNewLine(wordArray[i]);
    }

    Node *tree = NULL;
    optimizeTree(&tree, wordArray, 0, numLines);

    printf("\nStart spell check test 1\n");
    char *ALTFILENAME = "../PEXs/PEX3/test1.txt";
    printf("\nUsing words from test dictionary: %s\n", ALTFILENAME);

    int testNumLines = getNumLines(ALTFILENAME) - 1;
    char **testArray = malloc(sizeof(char *) * testNumLines);
    getWords(ALTFILENAME, testArray, testNumLines);
    for (int j = 0; j < testNumLines; ++j) {
        printf("%s is spelled correctly or in the dictionary: %d\n", testArray[j], searchTree(&tree, testArray[j]));
    }
    printf("\nStart spell check test 2\n");
    ALTFILENAME = "../PEXs/PEX3/test2.txt";
    printf("\nUsing words from test dictionary: %s\n", ALTFILENAME);

    testNumLines = getNumLines(ALTFILENAME) - 1;
    testArray = malloc(sizeof(char *) * testNumLines);
    getWords(ALTFILENAME, testArray, testNumLines);
    for (int j = 0; j < testNumLines; ++j) {
        printf("%s is spelled correctly or in the dictionary: %d\n", testArray[j], searchTree(&tree, testArray[j]));
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to insert words from a file into an array of strings, this is then used in other
 * functions to compare the strings read in here to those in a ternary tree. This is different from the ternaryTree library function
 * insertWordArray as this function does not need to return.
 * @param
 * This function takes:
 * char filename[]: which is the address of the file with the words to be read in
 * char **wordArray: the pointer to the array of strings to contain the words read from the file
 * int numLines: the number of lines in the file above
 * @return
 * This function returns nothing
 */
int getWords(char filename[], char **wordArray, int numLines) {
    FILE *fp = fopen(filename, "r");

    // Check if file exists
    if (fp == NULL) {
        printf("Could not open file %s", filename);
        return 0;
    }

    for (int i = 0; i < numLines; ++i) {
        wordArray[i] = malloc(sizeof(char *) * 50);
        fscanf(fp, "%s", wordArray[i]);
    }
    // Close the file
    if (fclose(fp) != 0) {
        printf("Failure to chooch\n");
    }

}
