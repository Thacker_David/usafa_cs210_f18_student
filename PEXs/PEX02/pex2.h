/** PEX2.h
* ==============================================================
* Name: David Thacker, 100918
* Section: T3-4
* Project: PEX 2 - Genome Sequencing (The Worst)
* Documentation Statement: Help Recieved from C3C Manny Riolo on the proper use of strcpy and
* removal of a null terminator.
* ==============================================================
* Instructions:
 * Complete and submit PEX2 as outlined in the writeup.
*/
#ifndef MYEXE_PEX02_H
#define MYEXE_PEX02_H

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define HUMANDNA "CGCAAATTTGCCGGATTTCCTTTGCTGTTCCTGCATGTAGTTTAAACGAGATTGCCAGCACCGGGTATCATTCACCATTTTTCTTTTCGTTAACTTGCCGTCAGCCTTTTCTTTGACCTCTTCTTTCTGTTCATGTGTATTTGCTGTCTCTTAGCCCAGACTTCCCGTGTCCTTTCCACCGGGCCTTTGAGAGGTCACAGGGTCTTGATGCTGTGGTCTTCATCTGCAGGTGTCTGACTTCCAGCAACTGCTGGCCTGTGCCAGGGTGCAGCTGAGCACTGGAGTGGAGTTTTCCTGTGGAGAGGAGCCATGCCTAGAGTGGGATGGGCCATTGTTCATG"
#define MOUSEDNA "CGCAATTTTTACTTAATTCTTTTTCTTTTAATTCATATATTTTTAATATGTTTACTATTAATGGTTATCATTCACCATTTAACTATTTGTTATTTTGACGTCATTTTTTTCTATTTCCTCTTTTTTCAATTCATGTTTATTTTCTGTATTTTTGTTAAGTTTTCACAAGTCTAATATAATTGTCCTTTGAGAGGTTATTTGGTCTATATTTTTTTTTCTTCATCTGTATTTTTATGATTTCATTTAATTGATTTTCATTGACAGGGTTCTGCTGTGTTCTGGATTGTATTTTTCTTGTGGAGAGGAACTATTTCTTGAGTGGGATGTACCTTTGTTCTTG"
#define CATDNA "CGCATTTTTGCCGGTTTTCCTTTGCTGTTTATTCATTTATTTTAAACGATATTTATATCATCGGGTTTCATTCACTATTTTTCTTTTCGATAAATTTTTGTCAGCATTTTCTTTTACCTCTTCTTTCTGTTTATGTTAATTTTCTGTTTCTTAACCCAGTCTTCTCGATTCTTATCTACCGGACCTATTATAGGTCACAGGGTCTTGATGCTTTGGTTTTCATCTGCAAGAGTCTGACTTCCTGCTAATGCTGTTCTGTGTCAGGGTGCATCTGAGCACTGATGTGGAGTTTTCTTGTGGATATGAGCCATTCATAGTGTGGGATGTGCCATAGTTCATG"

/**   ----------------------------------------------------------
 * This function contains the code needed to calculate the hamming distance between two strings. The hamming distance is
 * a measure of similarity between to strings. A lower hamming score represents strings that are more similar to each other
 * @param
 * This function takes a general str1 and str2 to be compared to find the hamming distance
 * @return
 * This function returns the calculated hamming distance
 */
int hammingDistance(char *str1, char *str2);

/**   ----------------------------------------------------------
 * This function contains the code needed to calculate the similarity between two strings. The similarity is a measure
 * of how similar two strings are to each other. For this two sequences with a high similarity score are more similar
 * than two sequences with a lower similarity score.
 * @param
 * This function takes a general str1 and str2 to be compared to find the similarity score
 * @return
 * This function returns the calculated similarity score
 */
float similarityScore(char *seq1, char *seq2);

/**   ----------------------------------------------------------
 * This function contains the code needed to count the number of matches above a certain threshold when comparing a
 * string to a constant comparison string, using the similarity score as a numeric measurement of how much they match.
 * @param
 * This function takes a string called genome, or the string that is being checked, a string called seq, or what genome
 * will be compared against, and a float called minScore, which is the threshold that the similarity score must pass to
 * be counted.
 * @return
 * This function returns the number of matches detected above the threshold.
 */

int countMatches(char *genome, char *seq, float minScore);

/**   ----------------------------------------------------------
 * This function contains the code needed find the highest similarity score overall when comparing a generic sequence to
 * a constant comparison string.
 * @param
 * This function takes a string called genome, or the string that is being checked, a string called seq, or what genome
 * will be compared against.
 * @return
 * This function returns the highest similarity score detected.
 */

float findBestMatch(char *genome, char *seq);

/**   ----------------------------------------------------------
 * This function contains the code needed find the string most like an unknown sequence, using the findBestMatch function's
 * output as a numeric comparison
 * @param
 * This function takes three strings, genome1, genome2, and genome3 which will all be compared against a 4th string,
 * unknownSeq
 * @return
 * This function returns a 1, 2, 3, or 0. A return of 1-3 represents the number of the string most like the unknownSeq,
 * with 0 being returned in the case of a tie between two or more of the genomes passed to the function.
 */
int findBestGenome(char *genome1, char *genome2, char *genome3, char *unknownSeq);

#endif //MYEXE_PEX02_H
