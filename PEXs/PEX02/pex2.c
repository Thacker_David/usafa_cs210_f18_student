/** PEX2.c
* ==============================================================
* Name: David Thacker, 100918
* Section: T3-4
* Project: PEX 2 - Genome Sequencing (The Worst)
* Documentation Statement: Help Recieved from C3C Manny Riolo on the proper use of strcpy and
* removal of a null terminator.
* ==============================================================
* Instructions:
 * Complete and submit PEX2 as outlined in the writeup.
*/

#include "pex2.h"

int main() {
    printf("hamming = %d\n", hammingDistance("AAA", "CC")); // result= -1
    printf("hamming = %d\n", hammingDistance("ACCT", "ACCG")); //result= 1
    printf("hamming = %d\n", hammingDistance("ACGGT", "CCGTT")); //result= 2
    printf("simularity = %0.3f\n", similarityScore("CCC", "CCC")); // result= 1.0
    printf("simularity = %0.3f\n", similarityScore("ACCT", "ACCG")); // result= 0.75
    printf("simularity = %0.3f\n", similarityScore("ACGGT", "CCGTT")); // result= 0.6
    printf("simularity = %0.3f\n", similarityScore("CCGCCGCCGA", "CCTCCTCCTA")); // result= 0.7
    printf("# matches = %d\n", countMatches("CCGCCGCCGA", "TTT", 0.6)); // result= 0
    printf("# matches = %d\n", countMatches("CCGCCGCCGA", "CCG", 0.2)); // result= 8
    printf("# matches = %d\n", countMatches(MOUSEDNA, "CGCTT", 0.5)); // result= 36
    printf("# matches = %d\n", countMatches(HUMANDNA, "CGC", 0.3)); // result= 215
    printf("best match = %0.3f\n", findBestMatch("CCGCCGCCGA", "TTT")); // result= 0.0
    printf("best match = %0.3f\n", findBestMatch("CTGCCACCAA", "CCGC")); // result= 0.75
    printf("best match = %0.3f\n", findBestMatch(MOUSEDNA, "CGCTT")); // result= 0.8
    printf("best match = %0.3f\n", findBestMatch(HUMANDNA, "CGC")); // result= 1.0
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAGGCCGGTT")); //result= 1
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAGGCCGGG")); //result= 2
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTAATTCTTTT")); //result= 3
    printf("likely genome match = %d.\n", findBestGenome(CATDNA, HUMANDNA, MOUSEDNA, "CTTTAG")); //result= 0
}

/**   ----------------------------------------------------------
 * This function contains the code needed to calculate the hamming distance between two strings. The hamming distance is
 * a measure of similarity between to strings. A lower hamming score represents strings that are more similar to each other.
 * @param
 * This function takes a general str1 and str2 to be compared to find the hamming distance.
 * @return
 * This function returns the calculated hamming distance.
 */
int hammingDistance(char *str1, char *str2) {
    if (strlen(str1) != strlen(str2)) {
        return -1;
    } else {
        int count = 0;
        for (int i = 0; i < strlen(str1); i++) {
            if (str1[i] != str2[i]) {
                count++;
            }
        }
        return count;
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to calculate the similarity between two strings. The similarity is a measure
 * of how similar two strings are to each other. For this two sequences with a high similarity score are more similar
 * than two sequences with a lower similarity score.
 * @param
 * This function takes a general str1 and str2 to be compared to find the similarity score.
 * @return
 * This function returns the calculated similarity score.
 */
float similarityScore(char *seq1, char *seq2) {
    if (strlen(seq1) != strlen(seq2)) {
        return -1;
    } else {
        return (((double) strlen(seq1) - hammingDistance(seq1, seq2)) / strlen(seq1));
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to count the number of matches above a certain threshold when comparing a
 * string to a constant comparison string, using the similarity score as a numeric measurement of how much they match.
 * @param
 * This function takes a string called genome, or the string that is being checked, a string called seq, or what genome
 * will be compared against, and a float called minScore, which is the threshold that the similarity score must pass to
 * be counted.
 * @return
 * This function returns the number of matches detected above the threshold.
 */
int countMatches(char *genome, char *seq, float minScore) {
    int count = 0;
    int copyAmount = strlen(seq);
    char tempCopy[copyAmount];
    int totalThrough = 0;
    do {
        for (int i = 0; i < strlen(genome); i++) {
            strncpy(tempCopy, &genome[i], copyAmount);

            if (similarityScore(tempCopy, seq) > minScore) {
                count++;
            }
            totalThrough++;
        }
    } while (totalThrough < strlen(genome));

    return count;
}

/**   ----------------------------------------------------------
 * This function contains the code needed find the highest similarity score overall when comparing a generic sequence to
 * a constant comparison string.
 * @param
 * This function takes a string called genome, or the string that is being checked, a string called seq, or what genome
 * will be compared against.
 * @return
 * This function returns the highest similarity score detected.
 */
float findBestMatch(char *genome, char *seq) {
    float highestScore = 0;
    float tempScore = 0;
    int copyAmount = strlen(seq);
    char tempCopy[copyAmount];
    int totalThrough = 0;
    do {
        for (int i = 0; i < strlen(genome); i++) {

            strncpy(tempCopy, &genome[i], copyAmount);
            tempCopy[strlen(seq)] = '\0';
            tempScore = similarityScore(tempCopy, seq);

            if (tempScore > highestScore) {
                highestScore = tempScore;
            }

            totalThrough++;
            tempScore = 0;
        }
    } while (totalThrough < strlen(genome));

    return highestScore;
}

/**   ----------------------------------------------------------
 * This function contains the code needed find the string most like an unknown sequence, using the findBestMatch function's
 * output as a numeric comparison
 * @param
 * This function takes three strings, genome1, genome2, and genome3 which will all be compared against a 4th string,
 * unknownSeq
 * @return
 * This function returns a 1, 2, 3, or 0. A return of 1-3 represents the number of the string most like the unknownSeq,
 * with 0 being returned in the case of a tie between two or more of the genomes passed to the function.
 */
int findBestGenome(char *genome1, char *genome2, char *genome3, char *unknownSeq) {
    float firstMatch = findBestMatch(genome1, unknownSeq);
    float secondMatch = findBestMatch(genome2, unknownSeq);
    float thirdMatch = findBestMatch(genome3, unknownSeq);

    if (firstMatch > secondMatch && firstMatch > thirdMatch) {
        return 1;
    } else if (secondMatch > firstMatch && secondMatch > thirdMatch) {
        return 2;
    } else if (thirdMatch > firstMatch && thirdMatch > secondMatch) {
        return 3;
    } else {
        return 0;
    }
}