/** PEX1.c
* ==============================================================
* Name: David Thacker, 090818
* Section: T3-4
* Project: PEX 1 - PIGS!
* Documentation Statement: No help recieved.
* ==============================================================
* Instructions:
 * Complete and submit PEX1 as outlined in the writeup.
*/

#include "pex1.h"

int gameOver = 0;
int turnCount = 0;
int maxPoints = 100;
int turnTimeout = 40;
int score0 = 0;
int score1 = 0;
int userInput;
int player;


int main() {
    testFunctions();
    //player controlled game start
    printf("Pig!\n");
    //loops between player turns until a)40 turns passed or b)one player reaches 100pts
    while (gameOver == 0) {
        playerZeroTurn();
        playerOneTurn();
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed for the autograde portion of PEX1, no help received
 * @param
 * This function takes no parameters
 * @return
 * This function returns nothing
 */
void testFunctions() {
    int numPips;
    int die;
    int player;
    int currentTotal;
    int score0;
    int score1;
    printf("Pig!\n\n");
    printf("TEST drawDie()\n");
    printf("Please enter a dice number:\n");
    scanf("%d", &numPips);
    drawDie(numPips);
    printf("TEST turnStatus()\n");
    printf("Please enter pips player total 0 score 1 score\n");
    scanf("%d %d %d %d %d", &die, &player, &currentTotal, &score0, &score1);
    turnStatus(die, player, currentTotal, score0, score1);
    printf("TEST gameStatus()\n");
    gameStatus(player, score0, score1);
}

/**   ----------------------------------------------------------
 * This function contains the code needed to print a die to the terminal between 1-6, no help received
 * @param
 * This function takes 1 parameter, pips, that tells the function which type of die to draw
 * @return
 * This function returns nothing
 */
void drawDie(int pips) {
    if (pips == 1) {
        printf(" ------- \n|       |\n|   *   |\n|       |\n ------- \n");
    } else
        printf(pips == 2 ? " ------- \n| *     |\n|       |\n|     * |\n ------- \n" : pips == 3
                                                                                       ? " ------- \n| *     |\n|   *   |\n|     * |\n ------- \n"
                                                                                       : pips == 4
                                                                                         ? " ------- \n *   * |\n|       |\n *   * |\n ------- \n"
                                                                                         : pips == 5
                                                                                           ? " ------- \n| *   * |\n|   *   |\n| *   * |\n ------- \n"
                                                                                           : " ------- \n| *   * |\n| *   * |\n| *   * |\n ------- \n");
}

/**   ----------------------------------------------------------
 * This function contains the code responsible for generating a random number between 1&6 inclusive
 * the random function is not seeded, no help received
 * @param
 * This function takes no parameters
 * @return
 * This function returns a random number between 1 and 6
 */
int rollDie() {
    return rand() % (6 + 1 - 1) + 1;
}

/**   ----------------------------------------------------------
 * This function contains the code responsible for determining the current players turn, if they have pigged out
 *and prints the results to the terminal, no help received
 * @param
 * This function takes 5 parameters, die, the last # rolled by rollDie(), player, or the player whos turn it currently
 * is, currentTotal, or the current turn score of that player, and score and score1, which store the game total score for
 * their respective players.
 * @return
 * This function returns nothing
 */
void turnStatus(int die, int player, int currentTotal, int score0, int score1) {
    switch (player) {
        case 0:
            if (die != 1) {
                printf("Current score for player 0 = %d.\n", currentTotal);
                printf("Total score = %d.\n", score0);
            } else {
                printf("Player 0 pigged out.\n");
            }
            break;
        case 1:
            if (die != 1) {
                printf("Current score for player 1 = %d.\n", currentTotal);
                printf("Total score = %d.\n", score1);
            } else {
                printf("Player 1 pigged out.\n");
            }
            break;
    }
}

/**   ----------------------------------------------------------
 * This function writes the players score and compares them against each other and against the variable maxPoints
 * to determine when to exit the game, no help received
 * @param
 * This function takes 3 parameters, player, or the player who' s turn it currently is, and score and score1,
 * which store the game total score for their respective players.
 * @return
 * This function returns nothing
 */
void gameStatus(int player, int score0, int score1) {
    printf("Player 0 score: %d\nPlayer 1 score: %d\n", score0, score1);
    if (score0 > maxPoints && score0 > score1) {
        printf("Player 0 won!\n");
        gameOver = 1;
    }
    if (score1 > maxPoints && score1 > score0) {
        printf("Player 1 won!\n");
        gameOver = 1;
    }
    if (turnCount == turnTimeout) {
        if (score0 > score1) {
            printf("Player 0 won!\n");
            gameOver = 1;
        } else {
            printf("Player 1 won!\n");
            gameOver = 1;
        }
    }
    printf("It is Player %d turn.\n", player);

}

/**   ----------------------------------------------------------
 * This function adds the current score of the turn to the players total current score
 * @param
 * This function takes 2 parameters, score, or the player who' s turn it currently is total score, and turnTotal,
 * or the total amount of points score thus far in that turn
 * @return
 * This function returns the new score of the player
 */
int totalScore(int score, int turnTotal) {
    return score + turnTotal;
}

/**   ----------------------------------------------------------
 * This function contains the code responsible for executing all aspects of player 0's turn this function executes until
 * the player is pigged out or they chose to stop, no help received
 * @param
 * This function takes no parameters
 * @return
 * This function returns nothing
 */
void playerZeroTurn() {
    int die;
    int turnOver = 0;
    int turnTotal = 0;
    gameStatus(0, score0, score1);
    while (turnOver == 0) {
        if (gameOver == 1) {
            break;
        }
        printf("Do you want to continue rolling? 1 - yes, 0 - no\n");
        scanf(" %d", &userInput);
        if (userInput == 0) {
            score0 = totalScore(score0, turnTotal);
            player = 1;
            turnOver = 1;
            break;
        }
        die = rollDie();
        drawDie(die);
        if (die == 1) {
            turnOver = 1;
            player = 1;
            break;
        } else {
            turnTotal = turnTotal + die;
        }
        turnStatus(die, player, turnTotal, score0, score1);

    }
    turnCount++;
}

/**   ----------------------------------------------------------
 * This function contains the code responsible for executing all aspects of player 1's turn this function executes until
 * the player is pigged out or they chose to stop, no help received
 * @param
 * This function takes no parameters
 * @return
 * This function returns nothing
 */
void playerOneTurn() {
    int die;
    int turnOver = 0;
    int turnTotal = 0;
    gameStatus(1, score0, score1);
    while (turnOver == 0) {
        if (gameOver == 1) {
            break;
        }
        printf("Do you want to continue rolling? 1 - yes, 0 - no\n");
        scanf(" %d", &userInput);
        if (userInput == 0) {
            score1 = totalScore(score1, turnTotal);
            player = 0;
            turnOver = 1;
            break;
        }
        die = rollDie();
        drawDie(die);
        if (die == 1) {
            turnOver = 1;
        } else {
            turnTotal = turnTotal + die;
        }
        turnStatus(die, player, turnTotal, score0, score1);

    }
    turnCount++;
}