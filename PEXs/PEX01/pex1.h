/** PEX1.h
* ==============================================================
* Name: David Thacker, 090818
* Section: T3-4
* Project: PEX 1 - PIGS!
* Documentation Statement: No help recieved.
* ==============================================================
* Instructions:
 * Complete and submit PEX1 as outlined in the writeup.
*/

#ifndef MYEXE_PEX1_H
#define MYEXE_PEX1_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/**   ----------------------------------------------------------
 * This function contains the code needed for the autograde portion of PEX1, no help received
 * @param
 * This function takes no parameters
 * @return
 * This function returns nothing
 */

void drawDie(int pips);

/**   ----------------------------------------------------------
 * This function contains the code needed to print a die to the terminal between 1-6, no help received
 * @param
 * This function takes 1 parameter, pips, that tells the function which type of die to draw
 * @return
 * This function returns nothing
 */

int rollDie();

/**   ----------------------------------------------------------
 * This function contains the code responsible for generating a random number between 1&6 inclusive
 * the random function is not seeded, no help received
 * @param
 * This function takes no parameters
 * @return
 * This function returns a random number between 1 and 6
 */

void turnStatus(int die, int player, int currentTotal, int score0, int score1);

/**   ----------------------------------------------------------
 * This function contains the code responsible for determining the current players turn, if they have pigged out
 *and prints the results to the terminal, no help received
 * @param
 * This function takes 5 parameters, die, the last # rolled by rollDie(), player, or the player whos turn it currently
 * is, currentTotal, or the current turn score of that player, and score and score1, which store the game total score for
 * their respective players.
 * @return
 * This function returns nothing
 */

void gameStatus(int player, int score0, int score1);

/**   ----------------------------------------------------------
 * This function writes the players score and compares them against each other and against the variable maxPoints
 * to determine when to exit the game, no help received
 * @param
 * This function takes 3 parameters, player, or the player who' s turn it currently is, and score and score1,
 * which store the game total score for their respective players.
 * @return
 * This function returns nothing
 */

int totalScore(int score, int turnTotal);

/**   ----------------------------------------------------------
 * This function adds the current score of the turn to the players total current score
 * @param
 * This function takes 2 parameters, score, or the player who' s turn it currently is total score, and turnTotal,
 * or the total amount of points score thus far in that turn
 * @return
 * This function returns the new score of the player
 */

void testFunctions();

/**   ----------------------------------------------------------
 * This function contains the code responsible for executing all aspects of player 1's turn this function executes until
 * the player is pigged out or they chose to stop, no help received
 * @param
 * This function takes no parameters
 * @return
 * This function returns nothing
 */
void playerZeroTurn();

/**   ----------------------------------------------------------
 * This function contains the code responsible for executing all aspects of player 0's turn this function executes until
 * the player is pigged out or they chose to stop, no help received
 * @param
 * This function takes no parameters
 * @return
 * This function returns nothing
 */
void playerOneTurn();

#endif //MYEXE_PEX1_H
