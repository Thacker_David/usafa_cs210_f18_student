/** listAsLinkedList.h
* ==============================================================
* Name: David Thacker, 150219
* Section: T7
* Project: Snake
* Documentation Statement: Helped Manny Riolo on parallel linked lists, cont movement, and curses implementation
* ==============================================================
* Instructions:
 * Design, implement, and test, a emulation of snake using linked list ADT
 * Adhere to all programming standards and the snake rubric
*/

#ifndef USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
#define USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H

#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>
#include <locale.h>
#include <wchar.h>
#include <stdbool.h>
#include <time.h>
#include <locale.h>
#include <wchar.h>

typedef struct node {
    int data;
    struct node *next;
} Node;

typedef struct {
    Node *head;
    Node *tail;
    int size;
} LinkedList;

/**   ----------------------------------------------------------
 * This function contains the code needed to initialize a linked list.
 * @param
 * This function takes no inputs.
 * @return
 * This function returns a pointer to a struct LinkedList.
 */
LinkedList *createLinkedList();

/**   ----------------------------------------------------------
 * This function contains the code needed to initialize a node, a part of a linked list.
 * @param
 * This function takes 1 input, the data to be inserted into the new node.
 * @return
 * This function returns a pointer to a struct Node.
 */
Node *initializeNode(int newData);

/**   ----------------------------------------------------------
 * This function contains the code needed to delete all parts of a linked list, included the nodes within.
 * @param
 * This function takes 1 input, the pointer to the LinkedList list to be deleted.
 * @return
 * This function returns nothing.
 */
void deleteLinkedList(LinkedList *list);

/**   ----------------------------------------------------------
 * This function contains the code needed to append a new node onto the end of a LinkedList.
 * @param
 * This function takes 2 inputs, the pointer to the LinkedList list to be appended, and the data to be inserted into
 * the new node.
 * @return
 * This function returns nothing.
 */
void appendElementLinkedList(LinkedList *list, int newData);

/**   ----------------------------------------------------------
 * This function is useless, because you can just look at the size of the list from the inital pointer, but I guess you
 * could use this function to tell the length of a linked list if you enjoy typing more.
 * @param
 * This function takes 1 input, the pointer to the LinkedList list who's length is to be determined.
 * @return
 * This function returns the length of the list as an int, which is useless.
 */
int lengthOfLinkedList(LinkedList *list);

/**   ----------------------------------------------------------
 * This function contains the code needed to print all the elements of a linked list in order.
 * @param
 * This function takes 1 input, the pointer to the LinkedList list who's data is to be printed.
 * @return
 * This function returns nothing.
 */
void printLinkedList(LinkedList *list);

/**   ----------------------------------------------------------
 * This function contains the code needed to return the data contained by a node at a certain known position in a linked
 * list.
 * @param
 * This function takes 2 inputs, the pointer to a LinkedList list, and a position inside the linked list.
 * @return
 * This function returns the data present inside the linked list at that position, or if it doesn't exist, it returns a -1.
 */
int getElementLinkedList(LinkedList *list, int position);

/**   ----------------------------------------------------------
 * This function contains the code needed to change the data contained by a node at a certain known position in a linked
 * list.
 * @param
 * This function takes 3 inputs, the pointer to a LinkedList list, the position inside the linked list, and the newData
 * to be written into the node.
 * @return
 * This function returns nothing.
 */
void changeElementLinkedList(LinkedList *list, int position, int newData);

/**   ----------------------------------------------------------
 * This function contains the code needed to delete an element inside a linkedlist.
 * @param
 * This function takes 2 inputs, the pointer to a LinkedList list, and the position inside the linked list.
 * @return
 * This function returns nothing.
 */
void deleteElementLinkedList(LinkedList *list, int position);

/**   ----------------------------------------------------------
 * This function contains the code needed to insert an node inside a linkedlist.
 * @param
 * This function takes 3 inputs, the pointer to a LinkedList list, the position inside the linked list, and the newData
 * to be written into the node.
 * @return
 * This function returns nothing.
 */
void insertElementLinkedList(LinkedList *list, int position, int newData);

/**   ----------------------------------------------------------
 * This function contains the code needed to return node containing a certain piece of data at a unknown position inside
 * a linked list.
 * @param
 * This function takes 2 inputs, the pointer to a LinkedList list, and the data to search for inside the linked list
 * @return
 * This function returns a pointer to the node containing the desired data.
 */
int findElementLinkedList(LinkedList *list, int data);


#endif //USAFA_CS220_S18_STUDENT_LISTASLINKEDLIST_H
