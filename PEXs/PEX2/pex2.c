/** pex2.c
* ==============================================================
* Name: David Thacker, 150219
* Section: T7
* Project: Snake
* Documentation Statement: Helped Manny Riolo on parallel linked lists, cont movement, and curses implementation
* ==============================================================
* Instructions:
 * Design, implement, and test, a emulation of snake using linked list ADT
 * Adhere to all programming standards and the snake rubric
*/

#include "pex2.h"
#include "listAsLinkedList.h"

//TODO fix flicker on main screen
//TODO make keyboard more responsive while decreasing speed of the snake
int main() {

    //begins curses initialization
    setlocale(LC_ALL, ""); // for use of unicode characters
    initscr(); // Initialize the window
    start_color(); // enable color support
    use_default_colors(); // set default colors
    cbreak();
    curs_set(TRUE); // Don't display a cursor
    noecho();
    keypad(stdscr, TRUE); // allows use of arrow keys
    timeout(DELAY);
    for (int i = 0; i < 256; i++) {
        init_pair(i, i, -1); // pair #, color, terminal background
    }

    //begins snake game
    goGame();


}

/**   ----------------------------------------------------------
 * This function contains the code needed to display the window in curses/cygwin
 * @param
 * This function takes 4 inputs, the height of the window, the width of the window, the startx and starty positions of
 * the game
 * @return
 * This function returns the window created.
 */
WINDOW *create_newwin(int height, int width, int starty, int startx) {
    WINDOW *local_win;

    local_win = newwin(height, width, starty, startx);
    box(local_win, 0, 0);
    wrefresh(local_win);

    return local_win;
}

void destroy_win(WINDOW *local_win) {
    /* box(local_win, ' ', ' '); : This won't produce the desired
     * result of erasing the window. It will leave it's four corners
     * and so an ugly remnant of window.
     */
    wborder(local_win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    /* The parameters taken are
     * 1. win: the window on which to operate
     * 2. ls: character to be used for the left side of the window
     * 3. rs: character to be used for the right side of the window
     * 4. ts: character to be used for the top side of the window
     * 5. bs: character to be used for the bottom side of the window
     * 6. tl: character to be used for the top left corner of the window
     * 7. tr: character to be used for the top right corner of the window
     * 8. bl: character to be used for the bottom left corner of the window
     * 9. br: character to be used for the bottom right corner of the window
     */
    wrefresh(local_win);
    delwin(local_win);
}

/**   ----------------------------------------------------------
 * This function contains the code needed to begin the snake game, initialize the snake linked lists, and acts as the main
 * control function for movement and drawing
 * @param
 * This function takes no inputs
 * @return
 * This function returns a 1 on successful end of the game
 */
int goGame() {

    WINDOW *my_win;
    int ch;
    border(0, 0, 0, 0, 0, 0, 0, 0);
    int prevailingKeySelection = 0;
    //begins window start
    //apparently this shows up as -38 and -60 something so
    int starty = (80 - WIDTH) / 2;
    int startx = (24 - HEIGHT) / 2;
    refresh();

    //creates the linked lists for the x and y position of the snake
    LinkedList *snakeX = createLinkedList();
    LinkedList *snakeY = createLinkedList();
    //creates the head nodes for the snake centered in the map
    //TODO get this shit to properly display in the middle of the screen (the startY and startX refer to above)
    appendElementLinkedList(snakeX, WIDTH / 2);
    appendElementLinkedList(snakeY, HEIGHT / 2);

    //Generates food x and y and mallocs it
    food *currFood = malloc(sizeof(food));
    generateFood(snakeX, snakeY, currFood);

//    mvprintw(13, 10, "Food %d,%d", currFood->foodX, currFood->foodY);
    int score = 0;

    //begins actual map appearance
    my_win = create_newwin(HEIGHT, WIDTH, starty, startx);
    nodelay(my_win, TRUE);
    //F1 is included for testing purposes at this time and for ease of exit for the user
    while ((ch = getch()) != KEY_F(1)) {
        clear();
        refresh();
        border(0, 0, 0, 0, 0, 0, 0, 0);
        score = snakeX->size - 1;
        mvprintw(10, 10, "Score: %d", score);
        //These were included for testing purposes and will remain for debugging during grading
//        mvprintw(11, 10, "Snake %d,%d", snakeX->head->data, snakeY->head->data);
//        mvprintw(12, 10, "Food %d,%d", currFood->foodX, currFood->foodY);

        //keeps the movement direction constant
        //Yes I know that the movement sucks, dont know what to do about that tbh
        prevailingKeySelection = movementSelection(prevailingKeySelection, snakeX, snakeY);

        eating(snakeX, snakeY, currFood);
        if (collision(snakeX, snakeY) == -1) {
            break;
        }
        draw(snakeX, snakeY, currFood);

        // update window and delay before erase
        wrefresh(my_win);
        // sleep a short time to allow screen to be seen before redrawn
        usleep(DELAY);
    }

    //frees memory from linked lists
    deleteLinkedList(snakeX);
    deleteLinkedList(snakeY);

    //Waits for user input to exit
    mvprintw(10, 10, "Your Score was: %d", score);
    mvprintw(11, 10, "Press any key to exit");
    getch();

    endwin();


    return 1;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to draw on the screen the position and status of each cell
 * What is drawn is determined by a global unicode in the pex2.h file
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY
 * @return
 * This function returns nothing.
 */
void draw(LinkedList *snakeX, LinkedList *snakeY, food *currFood) {
    //This should cause only a head to be printed if the size is 1, which it should always be greater than or equal to
    if (snakeX->size == 1) {
        mvprintw(getElementLinkedList(snakeY, 0), getElementLinkedList(snakeX, 0), HEAD);
    } else {
        //prints the head first with a different value than the rest of the snake body
        mvprintw(getElementLinkedList(snakeY, 0), getElementLinkedList(snakeX, 0), HEAD);
        //prints the rest of the snake body
        for (int i = 1; i < snakeX->size; ++i) {
            mvprintw(getElementLinkedList(snakeY, i), getElementLinkedList(snakeX, i), TAIL);
        }
    }
    //prints the food on the screen
    mvprintw(currFood->foodY, currFood->foodX, FOOD);

}

/**   ----------------------------------------------------------
 * This function contains the code needed to select and continue in a user selected direction
 * The snake continues on the last selected movement until it
 *  a)Gets a new direction from the user
 *  b)Hits itself
 *  c)Hits a wall
 * Collisions are detected in a separate child function
 * @param
 * This function takes 3 inputs, the previous direction of the snake as a int, and the linked lists snakeX and snakeY
 * @return
 * This function returns either the previous prevailing direction of the snake or a new one based off the key input
 */
int movementSelection(int prevailingKeySelection, LinkedList *snakeX, LinkedList *snakeY) {
    int ch = getch();

    if (ch == KEY_UP) {
        prevailingKeySelection = 0;
        upMove(snakeX, snakeY);
    } else if (ch == KEY_DOWN) {
        prevailingKeySelection = 1;
        downMove(snakeX, snakeY);
    } else if (ch == KEY_LEFT) {
        prevailingKeySelection = 2;
        leftMove(snakeX, snakeY);
    } else if (ch == KEY_RIGHT) {
        prevailingKeySelection = 3;
        rightMove(snakeX, snakeY);
    } else {
        //This dies with a switch statement for literally 0 reasons but whatever
        if (prevailingKeySelection == 0) {
            upMove(snakeX, snakeY);
        }
        if (prevailingKeySelection == 1) {
            downMove(snakeX, snakeY);
        }
        if (prevailingKeySelection == 2) {
            leftMove(snakeX, snakeY);
        }
        if (prevailingKeySelection == 3) {
            rightMove(snakeX, snakeY);
        }
    }
    return prevailingKeySelection;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to update the x and y direction of a snake going up or -Y.
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY.
 * @return
 * This function returns nothing.
 */
void upMove(LinkedList *snakeX, LinkedList *snakeY) {
    //this looks to see if there is only a head and then moves it up by a amount MOVEX
    if (snakeY->size == 1) {
        changeElementLinkedList(snakeY, 0, (snakeY->head->data - MOVEX));
        changeElementLinkedList(snakeX, 0, (snakeX->head->data));
    } else {
        //Insert element next to head
        insertElementLinkedList(snakeY, 1, snakeY->head->data);
        insertElementLinkedList(snakeX, 1, snakeX->head->data);

        //Delete Tail
        //move tail to the previous second to last
        //I expect this to need to be size -1 but for now here it is.
        deleteElementLinkedList(snakeY, snakeY->size - 1);
        deleteElementLinkedList(snakeX, snakeX->size - 1);
        //increment head
        changeElementLinkedList(snakeY, 0, (snakeY->head->data - MOVEX));
        changeElementLinkedList(snakeX, 0, (snakeX->head->data));
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to update the x and y direction of a snake going down or +Y.
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY.
 * @return
 * This function returns nothing.
 */
void downMove(LinkedList *snakeX, LinkedList *snakeY) {
//this looks to see if there is only a head and then moves it up by a amount MOVEX
    if (snakeY->size == 1) {
        changeElementLinkedList(snakeY, 0, (snakeY->head->data + MOVEX));
        changeElementLinkedList(snakeX, 0, (snakeX->head->data));
    } else {
        //Insert element next to head
        insertElementLinkedList(snakeY, 1, snakeY->head->data);
        insertElementLinkedList(snakeX, 1, snakeX->head->data);

        //Delete Tail
        //move tail to the previous second to last
        //I expect this to need to be size -1 but for now here it is.
        deleteElementLinkedList(snakeY, snakeY->size - 1);
        deleteElementLinkedList(snakeX, snakeX->size - 1);
        //increment head
        changeElementLinkedList(snakeY, 0, (snakeY->head->data + MOVEX));
        changeElementLinkedList(snakeX, 0, (snakeX->head->data));
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to update the x and y direction of a snake going left or -X.
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY.
 * @return
 * This function returns nothing.
 */
void leftMove(LinkedList *snakeX, LinkedList *snakeY) {
    if (snakeY->size == 1) {
        changeElementLinkedList(snakeX, 0, (snakeX->head->data - MOVEY));
        changeElementLinkedList(snakeY, 0, (snakeY->head->data));
    } else {
        //Insert element next to head
        insertElementLinkedList(snakeX, 1, snakeX->head->data);
        insertElementLinkedList(snakeY, 1, snakeY->head->data);
        //Delete Tail
        //move tail to the previous second to last
        //I expect this to need to be size -1 but for now here it is.
        deleteElementLinkedList(snakeX, snakeX->size - 1);
        deleteElementLinkedList(snakeY, snakeY->size - 1);
        //increment head
        changeElementLinkedList(snakeX, 0, (snakeX->head->data - MOVEY));
        changeElementLinkedList(snakeY, 0, (snakeY->head->data));

    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to update the x and y direction of a snake going right or +X.
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY.
 * @return
 * This function returns nothing.
 */
void rightMove(LinkedList *snakeX, LinkedList *snakeY) {
    if (snakeY->size == 1) {
        changeElementLinkedList(snakeX, 0, (snakeX->head->data + MOVEY));
        changeElementLinkedList(snakeY, 0, (snakeY->head->data));

    } else {
        //Insert element next to head
        insertElementLinkedList(snakeX, 1, snakeX->head->data);
        insertElementLinkedList(snakeY, 1, snakeY->head->data);
        //Delete Tail
        //move tail to the previous second to last
        //I expect this to need to be size -1 but for now here it is.
        deleteElementLinkedList(snakeX, snakeX->size - 1);
        deleteElementLinkedList(snakeY, snakeY->size - 1);
        //increment head
        changeElementLinkedList(snakeX, 0, (snakeX->head->data + MOVEY));
        changeElementLinkedList(snakeY, 0, (snakeY->head->data));
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to detect if the snake has collided with the food, resulting in growth of the body
 * and an increase in the score.
 * @param
 * This function takes 3 inputs, the LinkedLists snakeX and snakeY, the stuct currFood which has the x and y position of
 * the food inside.
 * @return
 * This function returns nothing
 */
void eating(LinkedList *snakeX, LinkedList *snakeY, food *currFood) {
    if (snakeX->head->data == currFood->foodX && snakeY->head->data == currFood->foodY) {
        generateFood(snakeX, snakeY, currFood);
        appendElementLinkedList(snakeX, snakeX->tail->data + 1);
        appendElementLinkedList(snakeY, snakeY->tail->data + 1);
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to detect if the snake has collided with the wall or any part of its body, and
 * then end the game if this is true
 * @param
 * This function takes 2 inputs, the LinkedLists snakeX and snakeY.
 * The barriers of the screen are given by the #define WIDTH and HEIGHT in pex2.h
 * @return
 * This function returns a -1 if a collision is detected and a 0 for no collision
 */
int collision(LinkedList *snakeX, LinkedList *snakeY) {
    if (snakeX->head->data == WIDTH || snakeY->head->data == HEIGHT || snakeY->head->data == 0 ||
        snakeX->head->data == 0) {
        return -1;
    }
    for (int i = 1; i < snakeX->size; ++i) {
        if (getElementLinkedList(snakeX, i) == snakeX->head->data &&
            getElementLinkedList(snakeY, i) == snakeY->head->data) {
            return -1;
        }
    }
    return 0;
}

/**   ----------------------------------------------------------
 * This function contains the code needed generate the x and y coords of the food for the snake. This function also checks
 * to ensure that the food is not created on top of the snakes body
 * @param
 * This function takes 3 inputs, the LinkedLists snakeX and snakeY, and the food struct currFood.
 * @return
 * This function returns nothing, the food x and y are passed out via pointer
 */
void generateFood(LinkedList *snakeX, LinkedList *snakeY, food *currFood) {
    int randomNumberX = 0;
    int randomNumberY = 0;

    bool onSnakeX = true;
    bool onSnakeY = true;

    if (snakeX->size > 1) {
        do {
            //generates two random numbers between the min and the max
            randomNumberX = rand() % (MIN_VALUE - MAX_VALUE);
            randomNumberY = rand() % (MIN_VALUE - MAX_VALUE);

            //checks to see if the food is being generated on the body or the head of the snake
            for (int i = 1; i < snakeX->size; ++i) {
                if (randomNumberX != getElementLinkedList(snakeX, i) && randomNumberX != snakeX->head->data) {
                    onSnakeX = false;
                }
                if (randomNumberY != getElementLinkedList(snakeY, i) && randomNumberY != snakeY->head->data) {
                    onSnakeY = false;
                }
            }
        } while (onSnakeX == true && onSnakeY == true);
    } else {
        do {
            randomNumberX = rand() % (MIN_VALUE - MAX_VALUE);
            randomNumberY = rand() % (MIN_VALUE - MAX_VALUE);
            if (randomNumberX != snakeX->head->data) {
                onSnakeX = false;
            }
            if (randomNumberY != snakeY->head->data) {
                onSnakeY = false;
            }
        } while (onSnakeX == true && onSnakeY == true);
    }

    //sets the food's x and y coord's to the random numbers from above
    currFood->foodX = randomNumberX;
    currFood->foodY = randomNumberY;
}