/** listAsLinkedList.c
* ==============================================================
* Name: David Thacker, 150219
* Section: T7
* Project: Snake
* Documentation Statement: Helped Manny Riolo on parallel linked lists, cont movement, and curses implementation
* ==============================================================
* Instructions:
 * Design, implement, and test, a emulation of snake using linked list ADT
 * Adhere to all programming standards and the snake rubric
*/

#include "listAsLinkedList.h"

/**   ----------------------------------------------------------
 * This function contains the code needed to initialize a linked list.
 * @param
 * This function takes no inputs.
 * @return
 * This function returns a pointer to a struct LinkedList.
 */
LinkedList *createLinkedList() {
    LinkedList *list;
    list = malloc(sizeof(LinkedList));
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;

    return list;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to initialize a node, a part of a linked list.
 * @param
 * This function takes 1 input, the data to be inserted into the new node.
 * @return
 * This function returns a pointer to a struct Node.
 */
Node *initializeNode(int newData) {

    Node *newNode;
    newNode = malloc(sizeof(Node));
    newNode->data = newData;
    newNode->next = NULL;

    return newNode;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to delete all parts of a linked list, included the nodes within.
 * @param
 * This function takes 1 input, the pointer to the LinkedList list to be deleted.
 * @return
 * This function returns nothing.
 */
void deleteLinkedList(LinkedList *list) {
    Node *tempPtr = list->head;
    Node *start = NULL;

    while (tempPtr != NULL) {
        start = tempPtr;
        tempPtr = tempPtr->next;
        free(start);
    }
    free(list);
}

/**   ----------------------------------------------------------
 * This function contains the code needed to append a new node onto the end of a LinkedList.
 * @param
 * This function takes 2 inputs, the pointer to the LinkedList list to be appended, and the data to be inserted into
 * the new node.
 * @return
 * This function returns nothing.
 */
void appendElementLinkedList(LinkedList *list, int newData) {
    Node *newNode = initializeNode(newData);
    if (list->head == NULL) {
        list->head = newNode;
    } else {
        list->tail->next = newNode;
    }
    list->tail = newNode;
    list->size++;
}

/**   ----------------------------------------------------------
 * This function is useless, because you can just look at the size of the list from the inital pointer, but I guess you
 * could use this function to tell the length of a linked list if you enjoy typing more.
 * @param
 * This function takes 1 input, the pointer to the LinkedList list who's length is to be determined.
 * @return
 * This function returns the length of the list as an int, which is useless.
 */
int lengthOfLinkedList(LinkedList *list) {
    //why is this a function?
    return list->size;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to print all the elements of a linked list in order.
 * @param
 * This function takes 1 input, the pointer to the LinkedList list who's data is to be printed.
 * @return
 * This function returns nothing.
 */
void printLinkedList(LinkedList *list) {
    Node *start = list->head;
    Node *tempPointer = NULL;
    while (start != NULL) {
        start = tempPointer;
        tempPointer = tempPointer->next;
        printf("%d", start->data);
    }
}

/**   ----------------------------------------------------------
 * This function contains the code needed to return the data contained by a node at a certain known position in a linked
 * list.
 * @param
 * This function takes 2 inputs, the pointer to a LinkedList list, and a position inside the linked list.
 * @return
 * This function returns the data present inside the linked list at that position, or if it doesn't exist, it returns a -1.
 */
int getElementLinkedList(LinkedList *list, int position) {
    if (list->size == 0 || list->size <= position) {
        //checks must be run on output of function to ensure that this value is not taken as a literal data result.
        return -1;
    }
    int curPosition = 0;
    Node *tempPointer = list->head;
    while (curPosition < position) {
        curPosition++;
        tempPointer = tempPointer->next;
    }
    return tempPointer->data;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to change the data contained by a node at a certain known position in a linked
 * list.
 * @param
 * This function takes 3 inputs, the pointer to a LinkedList list, the position inside the linked list, and the newData
 * to be written into the node.
 * @return
 * This function returns nothing.
 */
void changeElementLinkedList(LinkedList *list, int position, int newData) {
    if (list->size == 0 || list->size <= position) {
        printf("The list is either empty or an error has occured (change)");
        exit(0);
    }
    int curPosition = 0;
    Node *tempPointer = list->head;
    while (curPosition < position) {
        curPosition++;
        tempPointer = tempPointer->next;
    }
    tempPointer->data = newData;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to delete an element inside a linkedlist.
 * @param
 * This function takes 2 inputs, the pointer to a LinkedList list, and the position inside the linked list.
 * @return
 * This function returns nothing.
 */
void deleteElementLinkedList(LinkedList *list, int position) {
    if (list->size == 0 || list->size <= position) {
        printf("The list is either empty or an error has occured (delete)");
        exit(0);
    }

    int curPosition = 0;
    Node *tempPtr = list->head;
    Node *prevPtr = NULL;

    while (curPosition < position) {
        curPosition++;
        prevPtr = tempPtr;
        tempPtr = tempPtr->next;
    }

    if (tempPtr == list->head && tempPtr == list->tail) {
        list->head = NULL;
        list->tail = NULL;
    } else if (tempPtr == list->head) {
        list->head = tempPtr->next;
    } else if (tempPtr == list->tail) {
        list->tail = prevPtr;
        list->tail->next = NULL;
    } else {
        prevPtr->next = tempPtr->next;
    }
    list->size--;
    free(tempPtr);
}

/**   ----------------------------------------------------------
 * This function contains the code needed to insert an node inside a linkedlist.
 * @param
 * This function takes 3 inputs, the pointer to a LinkedList list, the position inside the linked list, and the newData
 * to be written into the node.
 * @return
 * This function returns nothing.
 */
void insertElementLinkedList(LinkedList *list, int position, int newData) {
    int curPosition = 0;

    if (list->size < position) {
        printf("Error\n");
        exit(0);
    }

    Node *newNode = malloc(sizeof(Node));
    Node *tempPtr = list->head;
    Node *prevPtr = NULL;
    newNode->data = newData;
    newNode->next = NULL;

    while (tempPtr != NULL && curPosition < position) {
        curPosition++;
        prevPtr = tempPtr;
        tempPtr = tempPtr->next;
    }

    if (tempPtr == NULL) {
        list->head = newNode;
        list->tail = newNode;
    } else if (tempPtr == list->head) {
        list->head = newNode;
        newNode->next = tempPtr;
    } else {
        newNode->next = tempPtr;
        prevPtr->next = newNode;
    }
    list->size++;
}

/**   ----------------------------------------------------------
 * This function contains the code needed to return node containing a certain piece of data at a unknown position inside
 * a linked list.
 * @param
 * This function takes 2 inputs, the pointer to a LinkedList list, and the data to search for inside the linked list
 * @return
 * This function returns a pointer to the node containing the desired data.
 */
int findElementLinkedList(LinkedList *list, int data) {
    Node *tempPtr = list->head;
    int curPosition = 0;

    while (tempPtr != NULL && tempPtr->data != data) {
        curPosition++;
        tempPtr = tempPtr->next;
    }

    if (tempPtr != NULL) {
        return curPosition;
    }

    return -1;
}