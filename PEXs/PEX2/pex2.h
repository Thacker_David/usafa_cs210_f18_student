/** pex2.h
* ==============================================================
* Name: David Thacker, 150219
* Section: T7
* Project: Snake
* Documentation Statement: Helped Manny Riolo on parallel linked lists, cont movement, and curses implementation
* ==============================================================
* Instructions:
 * Design, implement, and test, a emulation of snake using linked list ADT
 * Adhere to all programming standards and the snake rubric
*/

#ifndef USAFA_CS220_S18_STUDENT_PEX2_H
#define USAFA_CS220_S18_STUDENT_PEX2_H

//Definitely dont need all these but it works so I'm not gonna touch it
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>
#include <locale.h>
#include <wchar.h>
#include <stdbool.h>
#include <time.h>
#include <locale.h>
#include <wchar.h>
#include "listAsLinkedList.h"

#define WIDTH 100
#define HEIGHT 100
#define DELAY 50
#define HEAD "@"
#define FOOD "f"
#define TAIL "o"
#define MIN_VALUE 10
#define MAX_VALUE 90
//Apparently making these any less than one results in movement in only the left direction
#define MOVEX 1
#define MOVEY 1

typedef struct {
    int foodX;
    int foodY;
} food;

/**   ----------------------------------------------------------
 * This function contains the code needed to display the window in curses/cygwin
 * @param
 * This function takes 4 inputs, the height of the window, the width of the window, the startx and starty positions of
 * the game
 * @return
 * This function returns the window created.
 */
WINDOW *create_newwin(int height, int width, int starty, int startx);

/**   ----------------------------------------------------------
 * This function contains the code needed to detect if the snake has collided with the food, resulting in growth of the body
 * and an increase in the score.
 * @param
 * This function takes 3 inputs, the LinkedLists snakeX and snakeY, the stuct currFood which has the x and y position of
 * the food inside.
 * @return
 * This function returns nothing
 */
void eating(LinkedList *snakeX, LinkedList *snakeY, food *currFood);

/**   ----------------------------------------------------------
 * This function contains the code needed to detect if the snake has collided with the wall or any part of its body, and
 * then end the game if this is true
 * @param
 * This function takes 2 inputs, the LinkedLists snakeX and snakeY.
 * The barriers of the screen are given by the #define WIDTH and HEIGHT in pex2.h
 * @return
 * This function returns a -1 if a collision is detected and a 0 for no collision
 */
int collision(LinkedList *snakeX, LinkedList *snakeY);

/**   ----------------------------------------------------------
 * This function contains the code needed to begin the snake game, initialize the snake linked lists, and acts as the main
 * control function for movement and drawing
 * @param
 * This function takes no inputs
 * @return
 * This function returns a 1 on successful end of the game
 */
int goGame();

/**   ----------------------------------------------------------
 * This function contains the code needed to draw on the screen the position and status of each cell
 * What is drawn is determined by a global unicode in the pex2.h file
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY
 * @return
 * This function returns nothing.
 */
void draw(LinkedList *snakeX, LinkedList *snakeY, food *currFood);

/**   ----------------------------------------------------------
 * This function contains the code needed to select and continue in a user selected direction
 * The snake continues on the last selected movement until it
 *  a)Gets a new direction from the user
 *  b)Hits itself
 *  c)Hits a wall
 * Collisions are detected in a separate child function
 * @param
 * This function takes 3 inputs, the previous direction of the snake as a int, and the linked lists snakeX and snakeY
 * @return
 * This function returns either the previous prevailing direction of the snake or a new one based off the key input
 */
int movementSelection(int prevailingKeySelection, LinkedList *snakeX, LinkedList *snakeY);

/**   ----------------------------------------------------------
 * This function contains the code needed to update the x and y direction of a snake going up or -Y.
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY.
 * @return
 * This function returns nothing.
 */
void upMove(LinkedList *snakeX, LinkedList *snakeY);

/**   ----------------------------------------------------------
 * This function contains the code needed to update the x and y direction of a snake going down or +Y.
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY.
 * @return
 * This function returns nothing.
 */
void downMove(LinkedList *snakeX, LinkedList *snakeY);

/**   ----------------------------------------------------------
 * This function contains the code needed to update the x and y direction of a snake going right or +X.
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY.
 * @return
 * This function returns nothing.
 */
void rightMove(LinkedList *snakeX, LinkedList *snakeY);

/**   ----------------------------------------------------------
 * This function contains the code needed to update the x and y direction of a snake going left or -X.
 * @param
 * This function takes 2 inputs, the linked lists snakeX and snakeY.
 * @return
 * This function returns nothing.
 */
void leftMove(LinkedList *snakeX, LinkedList *snakeY);

/**   ----------------------------------------------------------
 * This function contains the code needed generate the x and y coords of the food for the snake. This function also checks
 * to ensure that the food is not created on top of the snakes body
 * @param
 * This function takes 3 inputs, the LinkedLists snakeX and snakeY, and the food struct currFood.
 * @return
 * This function returns nothing, the food x and y are passed out via pointer
 */
void generateFood(LinkedList *snakeX, LinkedList *snakeY, food *currFood);

#endif //USAFA_CS220_S18_STUDENT_PEX2_H

